import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.applet.Applet;
import java.io.File;
import java.io.IOException;
 
 
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.sql.SQLException;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class Acceuil extends JPanel {
	
	private JButton joueurunjoueur;

	
  public Acceuil(){
	
 	
	JPanel panel= new JPanel();
	
	panel.setLayout (new BoxLayout( panel , BoxLayout.Y_AXIS ) ) ;
    
    joueurunjoueur = new JButton("Jouer 1 joueur");
    JButton joueurdeuxjoueur = new JButton("Jouer 2 joueur");
    JButton aide = new JButton("Aide");
    JButton meilleurscore = new JButton("Meilleur score");
    
    JPanel joueurunjoueurpanel= new JPanel();
    JPanel joueurdeuxjoueurpanel= new JPanel();
    JPanel aidepanel= new JPanel();
    JPanel meilleurscorepanel= new JPanel();
    
    	
		joueurunjoueur.setPreferredSize(new Dimension(200, 40));
		joueurdeuxjoueur.setPreferredSize(new Dimension(200, 40));
		aide.setPreferredSize(new Dimension(200,40));
		meilleurscore.setPreferredSize(new Dimension(200,40));
	
		joueurunjoueurpanel.setBackground( Color.black );
		joueurdeuxjoueurpanel.setBackground( Color.black );
		aidepanel.setBackground( Color.black );
		meilleurscorepanel.setBackground( Color.black );
		
    
    		//~ /* ActionBouton o = new ActionBouton (fenetre,this,null,null,null,null,null); */
		ListenerAcceuil o = new ListenerAcceuil(this,null);
		
		joueurunjoueur.setActionCommand("lienjouerunjoueur");
		joueurunjoueur.addActionListener(o);
    
    
    joueurunjoueurpanel.add(joueurunjoueur);
    joueurdeuxjoueurpanel.add(joueurdeuxjoueur);
    aidepanel.add(aide);
    meilleurscorepanel.add(meilleurscore);
    
    
    panel.add(joueurunjoueurpanel);
    panel.add(joueurdeuxjoueurpanel);
    panel.add(aidepanel);
    panel.add(meilleurscorepanel);
    
    panel.setBackground(Color.black);
    
	this.add(panel);
	this.setBackground(Color.black);

	}
	
	
		public static void main( String args[] ) throws SQLException{
	
			System.out.println("test_lancer");
			Acceuil acc = new Acceuil();
			JFrame acceuil = new JFrame();
			acceuil.setTitle("Puck Man 0.1");
			acceuil.setSize(500, 500);
			acceuil.setLocationRelativeTo(null);
			acceuil.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			acceuil.setBackground(Color.BLACK);	
			
			acceuil.add(acc);
			acceuil.setVisible(true);
		
		}
	
	

}


