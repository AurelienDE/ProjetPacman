

import javax.sound.sampled.Clip;

import javax.swing.*;
import java.awt.*;
import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.URL;


public class Controleur implements ActionListener,KeyListener{
    Game g;
    Pacman pc;

    Ghost ghostun;

    Ghost ghostdeux;
    Ghost ghosttrois;
    Ghost ghostquatre;

    Grille gr;
    IHMFin finihm;

 int compteurInteruptThread=0;


    boolean controlcompteurfantomeun=false;
    int compteurfantomeun=0;
    boolean controlcompteurfantomedeux=false;
    int compteurfantomedeux=0;
    boolean controlcompteurfantometrois=false;
    int compteurfantometrois=0;
    boolean controlcompteurfantomequatre=false;
    int compteurfantomequatre=0;

    int swapfantomeun=0;
    int swapfantomedeux=0;
    int swapfantometrois=0;
    int swapfantomequatre=0;
    boolean controlcompteurBouleinvincible =false;
    int compteurBouleinvincible=0;

    int tempsthreadfantome=120;

    ImageIcon lel;
    int compteur = 0;
    int interuptthread =0;
    Clip clip;
    Clip clip2;
    AudioInputStream audioIn2;
    AudioInputStream audioIn;
    int xbeginfantome=0;
    int ybeginfantome=0;
    public Controleur() {
        //pc=new Pacman(510,516,"Pacman");
        this.setGame("Pacman");

        pc = new Pacman(0,0, "Pacman");
        gr = new Grille(290, 470);
       // if(ControleurDebut.f.compteurscore!=0){
        gr.setGrille(g.niveau);
        //}
        //else{
     //       gr.setGrille();
        //}

        pc.setPosPacman(gr.casebegin.getPosX(),gr.casebegin.getPosY());
        ghostun = new Ghost(getGrille().getSizeCase()*9, getGrille().getSizeCase()*10, "Ghostun");
        ghostdeux = new Ghost(getGrille().getSizeCase()*9, getGrille().getSizeCase()*10, "Ghostdeux");
        ghosttrois = new Ghost(getGrille().getSizeCase()*9, getGrille().getSizeCase()*10, "Ghosttrois");
        ghostquatre = new Ghost(getGrille().getSizeCase()*9, getGrille().getSizeCase()*10, "Ghostquatre");

        System.out.println("SIZE PORTE :"+gr.casePorte.size());
        xbeginfantome = (gr.casePorte.get(gr.casePorte.size()/2).getPosX());
        ybeginfantome = (gr.casePorte.get(gr.casePorte.size()/2).getPosY()+gr.getSizeCase());
        ghostun.setPos(xbeginfantome,ybeginfantome);
        ghostdeux.setPos(xbeginfantome,ybeginfantome);
        ghosttrois.setPos(xbeginfantome,ybeginfantome);
        ghostquatre.setPos(xbeginfantome,ybeginfantome);




        //  gr=new Grille(1200,800);

        //   try {
//            g.addMusique("sounds/pacman_start.wav","start");
  //          g.addMusique("sounds/pacman_eat.wav","eatBoule");
    //        g.addMusique("sounds/pacman_siren.wav","siren");

           //   } catch (IOException e) {
           //     e.printStackTrace();
           //  } catch (UnsupportedAudioFileException e) {
           //       e.printStackTrace();
           //  } catch (LineUnavailableException e) {
           //      e.printStackTrace();
           //  }

    }

    public Pacman getPacman() {
        return this.pc;
    }

    public Ghost getGhostun() {
        return this.ghostun;
    }

    public Ghost getGhostdeux() {
        return this.ghostdeux;
    }

    public Ghost getGhosttrois() {
        return this.ghosttrois;
    }

    public Ghost getGhostquatre() {
        return this.ghostquatre;
    }


    public Grille getGrille() {
        return this.gr;
    }

    public void actionPerformed(ActionEvent e) {

    }

    public void keyPressed(KeyEvent e) {

        if(g.getStart()){
            if (e.getKeyCode() == KeyEvent.VK_RIGHT){
                pc.memoire=2;
            }
            if (e.getKeyCode() == KeyEvent.VK_LEFT){
                pc.memoire=4;
            }
            if (e.getKeyCode() == KeyEvent.VK_UP){
                pc.memoire=1;
            }
            if (e.getKeyCode() == KeyEvent.VK_DOWN){
                pc.memoire=3;
            }
        }
        if(e.getKeyCode() == KeyEvent.VK_P) {
            g.setStart(false);
        }

        // Invoked when a key has been pressed.
        if(e.getKeyCode() == KeyEvent.VK_S){
            g.setStart(true);
            new Thread() {
                public void run() {
                    try {
                        ControleurDebut.f.paneprincpal.remove(ControleurDebut.f.ready);
                        ControleurDebut.f.paneprincpal.validate();
                        ControleurDebut.f.paneprincpal.repaint();
                        //g.PlayMusique("start");
                        this.sleep(4000);
                        //g.PlayMusique("eatBoule");
                       // g.getMusique("eatBoule").setLoopPoints(0,7000);
                        // g.musique.get("eatBoule").setLoopPoints(0,7500);
                        // g.musique.get("eatBoule").loop(Clip.LOOP_CONTINUOUSLY);
                        // g.getMusique("siren").setFramePosition(0);
                        //  g.PlayMusique("siren");
//                        System.out.println(g.getMusique("siren").getFrameLength());
                        // g.getMusique("siren").setLoopPoints(7000,59000);
                        // g.getMusique("siren").loop(Clip.LOOP_CONTINUOUSLY);
                        //this.sleep(15000);
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }

                  /* Creer une classe Jeu ??? */
                    while (pc.getMort() == false  && g.getStart()) {
                        if(ControleurDebut.f.test.getComponentCount()==0 &&
                                ControleurDebut.f.bouleinvinciblepanel.getComponentCount()==0){
                            pc.Mort();
//                            g.StopMusique("siren");
 //                           g.StopMusique("eatBoule");
                            ControleurDebut.f.dispose();
                            try {
                                g.niveau+=1;
                                ControleurDebut.f=new Fenetre(ControleurDebut.f.compteurscore,gr.pointVie);
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            break;
                        }
                        //f.paint(g);
                        try {
                            this.sleep(120);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // A chaque instant la grille veut savoir ou est le pacman.
                        if(gr.getCasePacman().getType()==2){
                            if(gr.getCasePacman().getPosX()==getGrille().getSizeCase()){
                                int newPosX=gr.hauteur-(getGrille().getSizeCase()+((getGrille().getSizeCase())/2));
                                pc.setPosPacman(newPosX,pc.getPosY());
                            }
                            else{
                                int newPosX=getGrille().getSizeCase()+((getGrille().getSizeCase())/2);
                                pc.setPosPacman(newPosX,pc.getPosY());
                            }
                        }

                        // Gestion du temps de l'invincibilité
                       // System.out.println(compteurBouleinvincible);
                        if(controlcompteurBouleinvincible==true){
                            compteurBouleinvincible++;
                            if (compteurBouleinvincible==60){
                                pc.invincible=false;
                                compteurBouleinvincible=0;
                                controlcompteurBouleinvincible=false;
                                if (swapfantomeun!=4) {
                                    swapfantomeun = 2;
                                }
                                if (swapfantomedeux!=4) {
                                    swapfantomedeux = 2;
                                }
                                if (swapfantometrois!=4) {
                                    swapfantometrois = 2;
                                }
                                if (swapfantomequatre!=4) {
                                    swapfantomequatre = 2;
                                }
                            }
                        }

                        System.out.println(controlcompteurfantomeun);
                        System.out.println("compteur fantomeun : "+compteurfantomeun);



                        if(controlcompteurfantomeun==true){
                            compteurfantomeun++;
                            if (compteurfantomeun==60){
                                ghostun.mange=false;
                                compteurfantomeun=0;
                                controlcompteurfantomeun=false;
                                swapfantomeun=2;
                                ghostun.sortie=false;
                            }
                        }

                        if(controlcompteurfantomedeux==true){
                            compteurfantomedeux++;
                            if (compteurfantomedeux==60){
                                ghostdeux.mange=false;
                                compteurfantomedeux=0;
                                controlcompteurfantomedeux=false;
                                swapfantomedeux=2;
                                ghostdeux.sortie=false;
                            }
                        }

                        if(controlcompteurfantometrois==true){
                            compteurfantometrois++;
                            if (compteurfantometrois==60){
                                ghosttrois.mange=false;
                                compteurfantometrois=0;
                                controlcompteurfantometrois=false;
                                swapfantometrois=2;
                                ghosttrois.sortie=false;
                            }
                        }

                        if(controlcompteurfantomequatre==true){
                            compteurfantomequatre++;
                            if (compteurfantomequatre==60){
                                ghostquatre.mange=false;
                                compteurfantomequatre=0;
                                controlcompteurfantomequatre=false;
                                swapfantomequatre=2;
                                ghostquatre.sortie=false;
                            }
                        }


                        //System.out.println(pc);
                       // System.out.println(gr.getCasePacman());

                        // BOULES JAUNES GSTION
                        //compteur++;

                        // Si le pacman tombe sur un fantome et qu'il n'est pas invincible
                        if ((gr.getCasePacman()==gr.getCaseFantome(ghostun) || interuptthread==1 )&& (pc.invincible==false)
                                && (ghostun.mange==false)){

                            try {
//                                g.StopMusique("siren");
           //                     g.StopMusique("eatBoule");
                                pc.setDisableDirection(false);
                                // fenetrefin =new IHMFin(ControleurDebut.f.compteurscore);
                                // System.out.println("Pacman  :"+gr.getCasePacman());
                                //System.out.println("Fantome  :"+gr.getCaseFantome());
                                if (interuptthread==0 || compteurInteruptThread<4 ) {
                                    interuptthread = 1;
                                    compteurInteruptThread++;
                                }
                                else{
                                    interuptthread=0;
                                    compteurInteruptThread=0;
                                }


                                gr.pointVie--;
                                ControleurDebut.f.affvie.setText("<html><body><p><font color='white'>Point de vie : " + gr.pointVie + "</font></p></body></html>");
                                ControleurDebut.f.labPacman.setBounds(gr.casebegin.getPosX(), gr.casebegin.getPosY(), getGrille().getSizeCase(), getGrille().getSizeCase());
                                if (gr.pointVie == 2) {
                                    ControleurDebut.f.nombreviespanel.remove(ControleurDebut.f.lavietrois);
                                } else if (gr.pointVie == 1){
                                    ControleurDebut.f.nombreviespanel.remove(ControleurDebut.f.laviedeux);
                                }
                                else if (gr.pointVie == 0) {
                                    ControleurDebut.f.nombreviespanel.remove(ControleurDebut.f.lavie);
                                    pc.Mort();
                                    ControleurDebut.f.dispose();
                                    finihm = new IHMFin(ControleurDebut.f.compteurscore);
                                    break;
                                }

                                retourBasePacman();
                                //this.sleep(4000);
                                this.run();
                                break;


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        // Si le pacman tombe sur un fantome et qu'il est invincible
                        else if (gr.getCasePacman()==gr.getCaseFantome(ghostun) && pc.invincible==true && ghostun.mange==false){
                            if (interuptthread==0) {
                                interuptthread = 2;
                            }
                            else{
                                interuptthread=0;
                            }
                        }
                        gr.getCasePacman();
                        GestionBoule();

                        // Pacman mange une boule invincible
                        GestionBouleInvincible();

                        if(pc.getDisableDir()==true) {
                            try {
                                choixDirection(false);
                                swapImagePacman();
                                this.sleep(120);
                                pc.setDisableDirection(false);
                                GestionBoule();
                                GestionBouleInvincible();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        choixDirection(true);
                        swapImagePacman();
                        //choixDirection(true);
                        //swapImagePacman();
                      //  pc.setDisableDirection(false);

                       /* if(gr.pointVie==0){

                            pc.Mort();
                            ControleurDebut.f.dispose();
                            finihm = new IHMFin(ControleurDebut.f.compteurscore);
                        } */
                    }
                }
            }.start();

// Thread PREMIER FANTOME
            new Thread() {
                public void run() {
                  /* Creer une classe Jeu ??? */
                    try{
                        this.sleep(4000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    while (pc.getMort() == false  && g.getStart()) {
                        //f.paint(g);
                        try {

                            this.sleep(tempsthreadfantome);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Si le pacman tombe sur un fantome et qu'il n'est pas invincible
                        if ((gr.getCasePacman() == gr.getCaseFantome(ghostun) || interuptthread==1)&&(pc.invincible==false)
                                && (ghostun.mange==false)){

                            try {

                                if (interuptthread==0 || compteurInteruptThread<4 ) {
                                    interuptthread = 1;
                                    compteurInteruptThread++;
                                }
                                else{
                                    interuptthread=0;
                                    compteurInteruptThread=0;
                                }

                                ControleurDebut.f.labelghostun.setBounds(xbeginfantome, ybeginfantome, getGrille().getSizeCase(),getGrille().getSizeCase());
                                retourBaseFantomeun(ghostun);
                                ghostun.sortie=false;

                                this.sleep(4000);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                        // Si le pacman tombe sur un fantome et qu'il invincible et que le fantome n'a pas été mangé
                        else if ((  ( gr.getCasePacman()==gr.getCaseFantome(ghostun) )&&(pc.invincible==true)
                                && ghostun.mange==false ) || interuptthread==2){

                            try {
                                ControleurDebut.f.labelghostun.setBounds(xbeginfantome, ybeginfantome, getGrille().getSizeCase(), getGrille().getSizeCase());
                                retourBaseFantomeun(ghostun);
                                interuptthread = 0;
                                ghostun.mange = true;
                                swapfantomeun = 4;
                                controlcompteurfantomeun = true;
                                // GESTION DU SCORE
                                if ((ghostdeux.mange == true && ghosttrois.mange == false && ghostquatre.mange == false) ||
                                        (ghosttrois.mange == true && ghostdeux.mange == false && ghostquatre.mange == false) ||
                                        (ghostquatre.mange == true && ghostdeux.mange == false && ghosttrois.mange == false)) {
                                    ControleurDebut.f.compteurscore = ControleurDebut.f.compteurscore + 400;
                                    ControleurDebut.f.affscore.setText("Score :" + ControleurDebut.f.compteurscore);

                                } else if ((ghostdeux.mange == true && ghosttrois.mange == true && ghostquatre.mange == false) ||
                                        (ghostdeux.mange == true && ghostquatre.mange == true && ghostquatre.mange == false) ||
                                        (ghosttrois.mange == true && ghostquatre.mange == true && ghostdeux.mange == false)){

                                    ControleurDebut.f.compteurscore = ControleurDebut.f.compteurscore + 800;
                                    ControleurDebut.f.affscore.setText("Score :" + ControleurDebut.f.compteurscore);
                               }
                                else if (ghostdeux.mange == true && ghosttrois.mange == true && ghostquatre.mange == true){

                                    ControleurDebut.f.compteurscore = ControleurDebut.f.compteurscore + 1600;
                                    ControleurDebut.f.affscore.setText("Score :" + ControleurDebut.f.compteurscore);
                                }
                                else {

                                    ControleurDebut.f.compteurscore = ControleurDebut.f.compteurscore + 200;
                                    ControleurDebut.f.affscore.setText("Score :" + ControleurDebut.f.compteurscore);
                                }
                                swapImageFantomeMangeGhostun();
                                this.sleep(4000);

                            } catch (InterruptedException e1) {
                                e1.printStackTrace();
                            }

                        }
                        if (swapfantomeun==3){
                            swapImageFantomePeurGhostun();

                        }
                        else if(swapfantomeun==2){
                            swapImageFantomeBaseGhostun();

                        }
                        else if(swapfantomeun==4){
                            swapImageFantomeMangeGhostun();

                        }
                        choixpremierfantome();
                        ghostun.setDisableDirection(false);
                        ControleurDebut.f.labelghostun.setBounds(ghostun.getPosX(), ghostun.getPosY(), getGrille().getSizeCase(),getGrille().getSizeCase());
                    }
                }
            }.start();



            // Thread deuxieme FANTOME
            new Thread() {
                public void run() {
                  /* Creer une classe Jeu ??? */
                    try{
                        this.sleep(4000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    while (pc.getMort() == false  && g.getStart()) {
                        //f.paint(g);
                        try {

                            this.sleep(tempsthreadfantome);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Si le pacman tombe sur un fantome et qu'il n'est pas invincible
                        if ((gr.getCasePacman() == gr.getCaseFantome(ghostdeux) || interuptthread==1)&&(pc.invincible==false)
                                && (ghostdeux.mange==false)){

                            try {

                                if (interuptthread==0 || compteurInteruptThread<4  ) {
                                    interuptthread = 1;
                                    compteurInteruptThread++;
                                }
                                else{
                                    interuptthread=0;
                                    compteurInteruptThread=0;
                                }

                                ControleurDebut.f.labelghostdeux.setBounds(xbeginfantome, ybeginfantome, getGrille().getSizeCase(),getGrille().getSizeCase());
                                retourBaseFantomeun(ghostdeux);
                                ghostdeux.sortie=false;

                                this.sleep(4000);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                        // Si le pacman tombe sur un fantome et qu'il invincible et que le fantome n'a pas été mangé
                        else if ((  ( gr.getCasePacman()==gr.getCaseFantome(ghostdeux) )&&(pc.invincible==true)
                                && ghostdeux.mange==false ) || interuptthread==2){

                            try {
                                ControleurDebut.f.labelghostdeux.setBounds(xbeginfantome, ybeginfantome, getGrille().getSizeCase(), getGrille().getSizeCase());
                                retourBaseFantomeun(ghostdeux);
                                interuptthread=0;
                                ghostdeux.mange=true;
                                swapfantomedeux=4;
                                controlcompteurfantomedeux=true;
                                if ((ghostun.mange == true && ghosttrois.mange == false && ghostquatre.mange == false) ||
                                        (ghosttrois.mange == true && ghostun.mange == false && ghostquatre.mange == false) ||
                                        (ghostquatre.mange == true && ghostun.mange == false && ghosttrois.mange == false)) {
                                    ControleurDebut.f.compteurscore = ControleurDebut.f.compteurscore + 400;
                                    ControleurDebut.f.affscore.setText("Score :" + ControleurDebut.f.compteurscore);

                                } else if ((ghostun.mange == true && ghosttrois.mange == true && ghostquatre.mange == false) ||
                                        (ghostun.mange == true && ghostquatre.mange == true && ghostquatre.mange == false) ||
                                        (ghosttrois.mange == true && ghostquatre.mange == true && ghostun.mange == false)){

                                    ControleurDebut.f.compteurscore = ControleurDebut.f.compteurscore + 800;
                                    ControleurDebut.f.affscore.setText("Score :" + ControleurDebut.f.compteurscore);
                                }
                                else if (ghostun.mange == true && ghosttrois.mange == true && ghostquatre.mange == true){

                                    ControleurDebut.f.compteurscore = ControleurDebut.f.compteurscore + 1600;
                                    ControleurDebut.f.affscore.setText("Score :" + ControleurDebut.f.compteurscore);
                                }
                                else {

                                    ControleurDebut.f.compteurscore = ControleurDebut.f.compteurscore + 200;
                                    ControleurDebut.f.affscore.setText("Score :" + ControleurDebut.f.compteurscore);
                                }

                                swapImageFantomeMangeGhostdeux();
                                this.sleep(4000);

                            } catch (InterruptedException e1) {
                                e1.printStackTrace();
                            }

                        }
                        if (swapfantomedeux==3){
                            swapImageFantomePeurGhostdeux();

                        }
                        else if(swapfantomedeux==2){
                            swapImageFantomeBaseGhostdeux();

                        }
                        else if(swapfantomedeux==4){
                            swapImageFantomeMangeGhostdeux();

                        }
                        choixpremierfantomedeux();
                        ghostdeux.setDisableDirection(false);
                        ControleurDebut.f.labelghostdeux.setBounds(ghostdeux.getPosX(), ghostdeux.getPosY(), getGrille().getSizeCase(),getGrille().getSizeCase());
                    }
                }
            }.start();


            // Thread TROISIEME FANTOME
            new Thread() {
                public void run() {
                  /* Creer une classe Jeu ??? */
                    try{
                        this.sleep(4000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    while (pc.getMort() == false  && g.getStart()) {
                        //f.paint(g);
                        try {

                            this.sleep(tempsthreadfantome);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Si le pacman tombe sur un fantome et qu'il n'est pas invincible
                        if ((gr.getCasePacman() == gr.getCaseFantome(ghosttrois) || interuptthread==1)&&(pc.invincible==false)
                                && (ghosttrois.mange==false)){

                            try {

                                if (interuptthread==0 || compteurInteruptThread<4 ) {
                                    interuptthread = 1;
                                    compteurInteruptThread++;
                                }
                                else{
                                    interuptthread=0;
                                    compteurInteruptThread=0;
                                }

                                ControleurDebut.f.labelghosttrois.setBounds(xbeginfantome, ybeginfantome, getGrille().getSizeCase(),getGrille().getSizeCase());
                                retourBaseFantomeun(ghosttrois);
                                ghosttrois.sortie=false;

                                this.sleep(4000);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                        // Si le pacman tombe sur un fantome et qu'il invincible et que le fantome n'a pas été mangé
                        else if ((  ( gr.getCasePacman()==gr.getCaseFantome(ghosttrois) )&&(pc.invincible==true)
                                && ghosttrois.mange==false ) || interuptthread==2){

                            try {
                                ControleurDebut.f.labelghosttrois.setBounds(xbeginfantome, ybeginfantome, getGrille().getSizeCase(), getGrille().getSizeCase());
                                retourBaseFantomeun(ghosttrois);
                                interuptthread=0;
                                ghosttrois.mange=true;
                                swapfantometrois=4;
                                controlcompteurfantometrois=true;
                                if ((ghostun.mange == true && ghostdeux.mange == false && ghostquatre.mange == false) ||
                                        (ghostdeux.mange == true && ghostun.mange == false && ghostquatre.mange == false) ||
                                        (ghostquatre.mange == true && ghostun.mange == false && ghostdeux.mange == false)) {
                                    ControleurDebut.f.compteurscore = ControleurDebut.f.compteurscore + 400;
                                    ControleurDebut.f.affscore.setText("Score :" + ControleurDebut.f.compteurscore);

                                } else if ((ghostun.mange == true && ghostdeux.mange == true && ghostquatre.mange == false) ||
                                        (ghostun.mange == true && ghostquatre.mange == true && ghostquatre.mange == false) ||
                                        (ghostdeux.mange == true && ghostquatre.mange == true && ghostun.mange == false)){

                                    ControleurDebut.f.compteurscore = ControleurDebut.f.compteurscore + 800;
                                    ControleurDebut.f.affscore.setText("Score :" + ControleurDebut.f.compteurscore);
                                }
                                else if (ghostun.mange == true && ghostdeux.mange == true && ghostquatre.mange == true){

                                    ControleurDebut.f.compteurscore = ControleurDebut.f.compteurscore + 1600;
                                    ControleurDebut.f.affscore.setText("Score :" + ControleurDebut.f.compteurscore);
                                }
                                else {

                                    ControleurDebut.f.compteurscore = ControleurDebut.f.compteurscore + 200;
                                    ControleurDebut.f.affscore.setText("Score :" + ControleurDebut.f.compteurscore);
                                }
                                swapImageFantomeMangeGhostun();
                                this.sleep(4000);

                            } catch (InterruptedException e1) {
                                e1.printStackTrace();
                            }

                        }
                        if (swapfantometrois==3){
                            swapImageFantomePeurGhosttrois();

                        }
                        else if(swapfantometrois==2){
                            swapImageFantomeBaseGhosttrois();

                        }
                        else if(swapfantometrois==4){
                            swapImageFantomeMangeGhosttrois();

                        }
                        choixpremierfantometrois();
                        ghosttrois.setDisableDirection(false);
                        ControleurDebut.f.labelghosttrois.setBounds(ghosttrois.getPosX(), ghosttrois.getPosY(), getGrille().getSizeCase(),getGrille().getSizeCase());
                    }
                }
            }.start();

            // Thread QUATRIEME FANTOME
            new Thread() {
                public void run() {
                  /* Creer une classe Jeu ??? */
                    try{
                        this.sleep(4000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    while (pc.getMort() == false  && g.getStart()) {
                        //f.paint(g);
                        try {

                            this.sleep(tempsthreadfantome);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Si le pacman tombe sur un fantome et qu'il n'est pas invincible
                        if ((gr.getCasePacman() == gr.getCaseFantome(ghostquatre) || interuptthread==1)&&(pc.invincible==false)
                                && (ghostquatre.mange==false)){

                            try {

                                if (interuptthread==0 || compteurInteruptThread<4 ) {
                                    interuptthread = 1;
                                    compteurInteruptThread++;
                                }
                                else{
                                    interuptthread=0;
                                    compteurInteruptThread=0;
                                }

                                ControleurDebut.f.labelghostquatre.setBounds(xbeginfantome, ybeginfantome, getGrille().getSizeCase(),getGrille().getSizeCase());
                                retourBaseFantomeun(ghostquatre);
                                ghostquatre.sortie=false;

                                this.sleep(4000);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                        // Si le pacman tombe sur un fantome et qu'il invincible et que le fantome n'a pas été mangé
                        else if ((  ( gr.getCasePacman()==gr.getCaseFantome(ghostquatre) )&&(pc.invincible==true)
                                && ghostquatre.mange==false ) || interuptthread==2){

                            try {
                                ControleurDebut.f.labelghostquatre.setBounds(xbeginfantome, ybeginfantome, getGrille().getSizeCase(), getGrille().getSizeCase());
                                retourBaseFantomeun(ghostquatre);
                                interuptthread=0;
                                ghostquatre.mange=true;
                                swapfantomequatre=4;
                                controlcompteurfantomequatre=true;
                                if ((ghostun.mange == true && ghostdeux.mange == false && ghosttrois.mange == false) ||
                                        (ghostdeux.mange == true && ghostun.mange == false && ghosttrois.mange == false) ||
                                        (ghosttrois.mange == true && ghostun.mange == false && ghostdeux.mange == false)) {
                                    ControleurDebut.f.compteurscore = ControleurDebut.f.compteurscore + 400;
                                    ControleurDebut.f.affscore.setText("Score :" + ControleurDebut.f.compteurscore);

                                } else if ((ghostun.mange == true && ghostdeux.mange == true && ghosttrois.mange == false) ||
                                        (ghostun.mange == true && ghosttrois.mange == true && ghosttrois.mange == false) ||
                                        (ghostdeux.mange == true && ghosttrois.mange == true && ghostun.mange == false)){

                                    ControleurDebut.f.compteurscore = ControleurDebut.f.compteurscore + 800;
                                    ControleurDebut.f.affscore.setText("Score :" + ControleurDebut.f.compteurscore);
                                }
                                else if (ghostun.mange == true && ghostdeux.mange == true && ghosttrois.mange == true){

                                    ControleurDebut.f.compteurscore = ControleurDebut.f.compteurscore + 1600;
                                    ControleurDebut.f.affscore.setText("Score :" + ControleurDebut.f.compteurscore);
                                }
                                else {

                                    ControleurDebut.f.compteurscore = ControleurDebut.f.compteurscore + 200;
                                    ControleurDebut.f.affscore.setText("Score :" + ControleurDebut.f.compteurscore);
                                }
                                swapImageFantomeMangeGhostun();
                                this.sleep(4000);

                            } catch (InterruptedException e1) {
                                e1.printStackTrace();
                            }

                        }
                        if (swapfantomequatre==3){
                            swapImageFantomePeurGhostquatre();

                        }
                        else if(swapfantomequatre==2){
                            swapImageFantomeBaseGhostquatre();

                        }
                        else if(swapfantomequatre==4){
                            swapImageFantomeMangeGhostquatre();

                        }
                        choixpremierfantomequatre();
                        ghostquatre.setDisableDirection(false);
                        ControleurDebut.f.labelghostquatre.setBounds(ghostquatre.getPosX(), ghostquatre.getPosY(), getGrille().getSizeCase(),getGrille().getSizeCase());
                    }
                }
            }.start();

        }

    }
    public void GestionBoule(){
        if (gr.getCasePacman().getType() == 0 &&
                gr.getCasePacman().getPosX() % getGrille().getSizeCase() == 0
                && gr.getCasePacman().getPosY() % getGrille().getSizeCase() ==0
                ) {
            pc.affamer=false;
            //   System.out.println(gr.getCasePacman());
            //  System.out.println(gr.caseLibre);
            int res = retrouveplaceboule();
//            if(!g.getMusique("eatBoule").isRunning()){
            //               try {
            //                  g.getMusique("eatBoule").setFramePosition(100);
            //                g.PlayMusique("eatBoule");
            //              g.getMusique("eatBoule").setLoopPoints(100,7500);
            //           g.getMusique("eatBoule").loop(Clip.LOOP_CONTINUOUSLY);
            //        } catch (IOException e) {
            //             e.printStackTrace();
            //         } catch (LineUnavailableException e) {
            //            e.printStackTrace();
            //        }
         //   }
          //if(!g.MusiqueEnCours("eatBoule")){
              //  System.out.println("SALUUUUUUUUUUUUUUUUUUUUUUUUUUUT");
//            try {
//                System.out.println(g.getMusique("eatBoule").getFramePosition());
//                if(g.getMusique("eatBoule").getFramePosition()>9000 || g.getMusique("eatBoule").getFramePosition()==0){
//                    g.getMusique("eatBoule").setFramePosition(0);
//                    g.PlayMusique("eatBoule");
//                }
//
//              //  g.getMusique("eatBoule").drain();
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (LineUnavailableException e) {
//                e.printStackTrace();
//            }

            //}
           // g.StopMusisque("eatBoule");


            //System.out.println(res);
            ControleurDebut.f.test.remove(res-1);
            ControleurDebut.f.compteurscore+=10;
            ControleurDebut.f.affscore.setText("<html><body><p><font color='white'>Score :" + ControleurDebut.f.compteurscore+"</font></p></body></html>");
            gr.getCaseLibre().remove(gr.getCasePacman());
            gr.getCasePacman().setType(10);
        }
        else if(!(gr.getCasePacman().getType() == 0 &&
                gr.getCasePacman().getPosX() % getGrille().getSizeCase() == 0
                && gr.getCasePacman().getPosY() % getGrille().getSizeCase() ==0
                ) && ( !pc.getDisableDir()) && pc.affamer==false){
            //g.StopMusique("eatBoule");
            pc.affamer=true;
        }
        else if(!(gr.getCasePacman().getType() == 0 &&
                gr.getCasePacman().getPosX() % getGrille().getSizeCase() == 0
                && gr.getCasePacman().getPosY() % getGrille().getSizeCase() ==0
        ) && ( !pc.getDisableDir()) && pc.affamer==true){
       //     g.StopMusique("eatBoule");
        }


    }
    public void GestionBouleInvincible(){
        if (gr.getCasePacman().getType() == 5 &&
                gr.getCasePacman().getPosX() % getGrille().getSizeCase() == 0
                && gr.getCasePacman().getPosY() % getGrille().getSizeCase() ==0
                ) {

            int res = retrouveplacebouleinvincible();
            ControleurDebut.f.bouleinvinciblepanel.remove(res -1);
            pc.invincible=true;
            controlcompteurBouleinvincible=true;
            if (swapfantomeun!=4) {
                swapfantomeun = 3;
            }
            if (swapfantomedeux!=4) {
                swapfantomedeux = 3;
            }
            if (swapfantometrois!=4) {
                swapfantometrois = 3;
            }
            if (swapfantomequatre!=4) {
                swapfantomequatre = 3;
            }
            compteurBouleinvincible=0;
            gr.casebouleinvincible.remove(gr.getCasePacman());
            gr.getCasePacman().setType(10);
            ControleurDebut.f.compteurscore+=50;
            ControleurDebut.f.affscore.setText("<html><body><p><font color='white'>Score :" + ControleurDebut.f.compteurscore+"</font></p></body></html>");
        }
    }
    public int retrouveplaceboule(){
        int comptcase=0;
        boolean masoeur=true;
        int dernierevar=0;
        for (Case cas : gr.getCaseLibre()) {
            if (masoeur==true) {
                comptcase++;
            }
            if (cas==gr.getCasePacman()){
                dernierevar=comptcase;
                masoeur=false;
            }
        }

        return dernierevar;
    }

    public int retrouveplacebouleinvincible(){
        int comptcase=0;
        boolean masoeur=true;
        int dernierevar=0;
        for (Case cas : gr.casebouleinvincible) {
            if (masoeur==true) {
                comptcase++;
            }
            if (cas==gr.getCasePacman()){
                dernierevar=comptcase;
                masoeur=false;
            }
        }

        return dernierevar;
    }

    public void choixpremierfantome() {

        if (ghostun.memoire==0) {
            if (ghostun.memoirecheminverse==4) {
                String direction = String.valueOf((int) (Math.random() * 3) + 2);
               // System.out.println(direction);
                Integer integerdirection = Integer.parseInt(direction);
                int intdirection = integerdirection;
                choixDirectionFantome(intdirection);

            }
            else {
                String direction = String.valueOf((int) (Math.random() * 4) + 1);
                Integer integerdirection = Integer.parseInt(direction);
                int intdirection = integerdirection;
                choixDirectionFantome(intdirection);

            }

        }
        else{
            choixDirectionFantome(ghostun.memoire);
        }

    }

    public void choixpremierfantomedeux() {

        if (ghostdeux.memoire==0) {
            if (ghostdeux.memoirecheminverse==4) {
                String direction = String.valueOf((int) (Math.random() * 3) + 2);
                // System.out.println(direction);
                Integer integerdirection = Integer.parseInt(direction);
                int intdirection = integerdirection;
                choixDirectionFantomedeux(intdirection);

            }
            else {
                String direction = String.valueOf((int) (Math.random() * 4) + 1);
                Integer integerdirection = Integer.parseInt(direction);
                int intdirection = integerdirection;
                choixDirectionFantomedeux(intdirection);

            }

        }
        else{
            choixDirectionFantomedeux(ghostdeux.memoire);
        }

    }

    public void choixpremierfantometrois() {

        if (ghosttrois.memoire==0) {
            if (ghosttrois.memoirecheminverse==4) {
                String direction = String.valueOf((int) (Math.random() * 3) + 2);
                // System.out.println(direction);
                Integer integerdirection = Integer.parseInt(direction);
                int intdirection = integerdirection;
                choixDirectionFantometrois(intdirection);

            }
            else {
                String direction = String.valueOf((int) (Math.random() * 4) + 1);
                Integer integerdirection = Integer.parseInt(direction);
                int intdirection = integerdirection;
                choixDirectionFantometrois(intdirection);

            }

        }
        else{
            choixDirectionFantometrois(ghosttrois.memoire);
        }

    }

    public void choixpremierfantomequatre() {

        if (ghostquatre.memoire==0) {
            if (ghostquatre.memoirecheminverse==4) {
                String direction = String.valueOf((int) (Math.random() * 3) + 2);
                // System.out.println(direction);
                Integer integerdirection = Integer.parseInt(direction);
                int intdirection = integerdirection;
                choixDirectionFantomequatre(intdirection);

            }
            else {
                String direction = String.valueOf((int) (Math.random() * 4) + 1);
                Integer integerdirection = Integer.parseInt(direction);
                int intdirection = integerdirection;
                choixDirectionFantomequatre(intdirection);

            }

        }
        else{
            choixDirectionFantomequatre(ghostquatre.memoire);
        }

    }

    public void retourBasePacman(){
        pc.setPos(gr.casebegin.getPosX(),gr.casebegin.getPosY());

    }

    public void retourBaseFantomeun(Ghost ghost){
        ghost.setPos(xbeginfantome,ybeginfantome);


    }

    public void swapImageFantomePeurGhostun() {
            ControleurDebut.f.ghostun.setImage(ControleurDebut.f.imageghostpeureu);
    }

    public void swapImageFantomeBaseGhostun() {
        ControleurDebut.f.ghostun.setImage(ControleurDebut.f.imageghostun);
    }

    public void swapImageFantomeMangeGhostun() {
        ControleurDebut.f.ghostun.setImage(ControleurDebut.f.imageghostmange);
    }

    public void swapImageFantomePeurGhostdeux() {
        ControleurDebut.f.ghostdeux.setImage(ControleurDebut.f.imageghostpeureu);
    }

    public void swapImageFantomeBaseGhostdeux() {
        ControleurDebut.f.ghostdeux.setImage(ControleurDebut.f.imageghostdeux);
    }

    public void swapImageFantomeMangeGhostdeux() {
        ControleurDebut.f.ghostdeux.setImage(ControleurDebut.f.imageghostmange);
    }

    public void swapImageFantomePeurGhosttrois() {
        ControleurDebut.f.ghosttrois.setImage(ControleurDebut.f.imageghostpeureu);
    }

    public void swapImageFantomeBaseGhosttrois() {
        ControleurDebut.f.ghosttrois.setImage(ControleurDebut.f.imageghosttrois);
    }

    public void swapImageFantomeMangeGhosttrois() {
        ControleurDebut.f.ghosttrois.setImage(ControleurDebut.f.imageghostmange);
    }

    public void swapImageFantomePeurGhostquatre() {
        ControleurDebut.f.ghostquatre.setImage(ControleurDebut.f.imageghostpeureu);
    }

    public void swapImageFantomeBaseGhostquatre() {
        ControleurDebut.f.ghostquatre.setImage(ControleurDebut.f.imageghostquatre);
    }

    public void swapImageFantomeMangeGhostquatre() {
        ControleurDebut.f.ghostquatre.setImage(ControleurDebut.f.imageghostmange);
    }


    public void swapImagePacman(){
        if(ControleurDebut.f.swap==false){
            ControleurDebut.f.labPacman.setBounds(pc.getPosX(),pc.getPosY(),getGrille().getSizeCase(),getGrille().getSizeCase());
            pc.setStringImageActuel(pc.getStringImageAcutel().replace('F','O'));
            ControleurDebut.f.pacman.setImage(pc.getImageActuel());
            ControleurDebut.f.swap=true;
        }
        else if(ControleurDebut.f.swap==true){
            ControleurDebut.f.labPacman.setBounds(pc.getPosX(),pc.getPosY(),getGrille().getSizeCase(),getGrille().getSizeCase());
            pc.setStringImageActuel(pc.getStringImageAcutel().replace('O','F'));
            ControleurDebut.f.pacman.setImage(pc.getImageActuel());
            ControleurDebut.f.swap=false;
        }
    }
    public void choixDirection(boolean b){
        if(b==true) {
            if (pc.memoire == 2) {
                if (gr.DeplacementDroiteOK(gr.getCasePacman())) {
                    pc.setDirection(2);
                }
            } else if (pc.memoire == 3) {
                if (gr.DeplacementBasOK(gr.getCasePacman())) {
                    pc.setDirection(3);
                }
            } else if (pc.memoire == 1) {
                if (gr.DeplacementHautOK(gr.getCasePacman())) {
                    pc.setDirection(1);
                }
            } else if (pc.memoire == 4) {
                if (gr.DeplacementGaucheOK(gr.getCasePacman())) {
                    pc.setDirection(4);
                }
            }
        }
        if( (pc.getDirection()==2 || pc.getDirection()==0)){

            if(gr.DeplacementDroiteOK(gr.getCasePacman())) {
                if(!pc.getStringImageAcutel().equals("PacmanDroiteO") || !pc.getStringImageAcutel().equals("PacmanDroiteF")){
                    int sizeactuel = pc.getStringImageAcutel().length();
                    pc.setStringImageActuel("PacmanDroite" + pc.getStringImageAcutel().substring(sizeactuel-1,sizeactuel));
                }
                pc.incrementePosX();
            }

        }
        if(pc.getDirection()==3){
            if(gr.DeplacementBasOK(gr.getCasePacman())) {
                if(!pc.getStringImageAcutel().equals("PacmanBasO") || !pc.getStringImageAcutel().equals("PacmanBasF")){
                    int sizeactuel = pc.getStringImageAcutel().length();
                    pc.setStringImageActuel("PacmanBas" + pc.getStringImageAcutel().substring(sizeactuel-1,sizeactuel));
               }
                pc.incrementePosY();
            }
        }
        if(pc.getDirection()==1){
            if(gr.DeplacementHautOK(gr.getCasePacman())) {
                if(!pc.getStringImageAcutel().equals("PacmanHautO") || !pc.getStringImageAcutel().equals("PacmanHautF")){
                    int sizeactuel = pc.getStringImageAcutel().length();
                    pc.setStringImageActuel("PacmanHaut"+pc.getStringImageAcutel().substring(sizeactuel-1,sizeactuel));
                    System.out.println(pc.getStringImageAcutel());
                }
                pc.decrementePosY();
            }
        }
        if(pc.getDirection()==4){
            if(gr.DeplacementGaucheOK(gr.getCasePacman())) {
                if(!pc.getStringImageAcutel().equals("PacmanGaucheO") || !pc.getStringImageAcutel().equals("PacmanGaucheF")){
                    int sizeactuel = pc.getStringImageAcutel().length();
                    pc.setStringImageActuel("PacmanGauche"+pc.getStringImageAcutel().substring(sizeactuel-1,sizeactuel));
                    System.out.println(pc.getStringImageAcutel());
                }
                pc.decrementePosX();
            }
        }

    }

    public void choixDirectionFantome(int choixdirfantome){
        if( choixdirfantome==1){
            if(gr.DeplacementDroiteFantomeOKun(gr.getCaseFantome(ghostun))) {
                ghostun.incrementePosX();
                ghostun.memoire=1;
            }
            else{
                ghostun.memoire=0;
                ghostun.memoirecheminverse=4;
                choixpremierfantome();

            }

        }
        if(choixdirfantome==2){
            if(gr.DeplacementBasFantomeOKun(gr.getCaseFantome(ghostun))) {
                ghostun.incrementePosY();
                ghostun.memoire=2;
            }
            else{
                ghostun.memoire=0;
                ghostun.memoirecheminverse=3;
                choixpremierfantome();

            }
        }
        if(choixdirfantome==3){
            if(gr.DeplacementHautFantomeOKun(gr.getCaseFantome(ghostun))) {
                ghostun.decrementePosY();
                ghostun.memoire=3;
            }
            else{
                ghostun.memoire=0;
                choixpremierfantome();

            }
        }
        if(choixdirfantome==4){
            if(gr.DeplacementGaucheFantomeOKun(gr.getCaseFantome(ghostun))) {
                ghostun.decrementePosX();
                ghostun.memoire=4;

            }
            else{
                ghostun.memoire=0;
                choixpremierfantome();

            }
        }

    }

    public void choixDirectionFantomedeux(int choixdirfantome){
        if( choixdirfantome==1){
            if(gr.DeplacementDroiteFantomeOKdeux(gr.getCaseFantome(ghostdeux))) {
                ghostdeux.incrementePosX();
                ghostdeux.memoire=1;
            }
            else{
                ghostdeux.memoire=0;
                ghostdeux.memoirecheminverse=4;
                choixpremierfantomedeux();

            }

        }
        if(choixdirfantome==2){
            if(gr.DeplacementBasFantomeOKdeux(gr.getCaseFantome(ghostdeux))) {
                ghostdeux.incrementePosY();
                ghostdeux.memoire=2;
            }
            else{
                ghostdeux.memoire=0;
                ghostdeux.memoirecheminverse=3;
                choixpremierfantomedeux();

            }
        }
        if(choixdirfantome==3){
            if(gr.DeplacementHautFantomeOKdeux(gr.getCaseFantome(ghostdeux))) {
                ghostdeux.decrementePosY();
                ghostdeux.memoire=3;
            }
            else{
                ghostdeux.memoire=0;
                choixpremierfantomedeux();

            }
        }
        if(choixdirfantome==4){
            if(gr.DeplacementGaucheFantomeOKdeux(gr.getCaseFantome(ghostdeux))) {
                ghostdeux.decrementePosX();
                ghostdeux.memoire=4;

            }
            else{
                ghostdeux.memoire=0;
                choixpremierfantomedeux();

            }
        }

    }

    //DIRECTION FANTOME TROIS

    public void choixDirectionFantometrois(int choixdirfantome){
        if( choixdirfantome==1){
            if(gr.DeplacementDroiteFantomeOKtrois(gr.getCaseFantome(ghosttrois))) {
                ghosttrois.incrementePosX();
                ghosttrois.memoire=1;
            }
            else{
                ghosttrois.memoire=0;
                ghosttrois.memoirecheminverse=4;
                choixpremierfantometrois();

            }

        }
        if(choixdirfantome==2){
            if(gr.DeplacementBasFantomeOKtrois(gr.getCaseFantome(ghosttrois))) {
                ghosttrois.incrementePosY();
                ghosttrois.memoire=2;
            }
            else{
                ghosttrois.memoire=0;
                ghosttrois.memoirecheminverse=3;
                choixpremierfantometrois();

            }
        }
        if(choixdirfantome==3){
            if(gr.DeplacementHautFantomeOKtrois(gr.getCaseFantome(ghosttrois))) {
                ghosttrois.decrementePosY();
                ghosttrois.memoire=3;
            }
            else{
                ghosttrois.memoire=0;
                choixpremierfantometrois();

            }
        }
        if(choixdirfantome==4){
            if(gr.DeplacementGaucheFantomeOKtrois(gr.getCaseFantome(ghosttrois))) {
                ghosttrois.decrementePosX();
                ghosttrois.memoire=4;

            }
            else{
                ghosttrois.memoire=0;
                choixpremierfantometrois();

            }
        }

    }

    public void choixDirectionFantomequatre(int choixdirfantome){
        if( choixdirfantome==1){
            if(gr.DeplacementDroiteFantomeOKquatre(gr.getCaseFantome(ghostquatre))) {
                ghostquatre.incrementePosX();
                ghostquatre.memoire=1;
            }
            else{
                ghostquatre.memoire=0;
                ghostquatre.memoirecheminverse=4;
                choixpremierfantomequatre();

            }

        }
        if(choixdirfantome==2){
            if(gr.DeplacementBasFantomeOKquatre(gr.getCaseFantome(ghostquatre))) {
                ghostquatre.incrementePosY();
                ghostquatre.memoire=2;
            }
            else{
                ghostquatre.memoire=0;
                ghostquatre.memoirecheminverse=3;
                choixpremierfantomequatre();

            }
        }
        if(choixdirfantome==3){
            if(gr.DeplacementHautFantomeOKquatre(gr.getCaseFantome(ghostquatre)) ){
                ghostquatre.decrementePosY();
                ghostquatre.memoire=3;
            }
            else{
                ghostquatre.memoire=0;
                choixpremierfantomequatre();

            }
        }
        if(choixdirfantome==4){
            if(gr.DeplacementGaucheFantomeOKquatre(gr.getCaseFantome(ghostquatre))) {
                ghostquatre.decrementePosX();
                ghostquatre.memoire=4;

            }
            else{
                ghostquatre.memoire=0;
                choixpremierfantomequatre();

            }
        }

    }


    public void keyReleased(KeyEvent e) {
    }
    public void keyTyped(KeyEvent e) {
        // Invoked when a key has been typed.
    }
    public Game getGame(){
        return this.g;
    }
    public void setGame(String s){
        this.g = new Game(s);
    }
}
