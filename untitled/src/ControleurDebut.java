

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;


public class ControleurDebut implements ActionListener,WindowListener {
    static Debut d;
    static Fenetre f;
    static Settings s;
    static FenetreEdit fe;
    static IHMFin fin;
    static IHMMeilleurScore mc;

    /**Constructeur*/
    public ControleurDebut(Debut d,Fenetre f, IHMFin fin, IHMMeilleurScore mc){
        this.d=d;
        this.f=f;
        this.fin=fin;
        this.mc=mc;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand() == "lienjouerunjoueur") {

            d.dispose();
            System.out.println("test_Accuelel");
            try {
               this.f = new Fenetre();
               //this.fe=new FenetreEdit();
           } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        if (e.getActionCommand()=="lienrejouer"){

            //fin.dispose();
            System.out.println("test_Accuelel");
                fin.dispose();
                d=new Debut();

        }

        if (e.getActionCommand()=="lienenvoyerscore"){

            try{
                String nomfichiertext = "Score.txt";

                PrintWriter fich;


                fich = new PrintWriter(new BufferedWriter(new FileWriter(nomfichiertext, true))); //true c'est elle qui permet d'écrire à la suite des donnée enregistrer et non de les remplac
                fich.println( "\n"+fin.pseudo.getText()+" : "+ fin.score);
                fich.close();

            } catch (Exception ee) {}

        }

        if (e.getActionCommand()=="lienmeilleurscore"){

            mc = new IHMMeilleurScore();
            d.dispose();

        }
        if(e.getActionCommand() == "editeur"){
            this.fe = new FenetreEdit();
        }
        if (e.getActionCommand() == "settings") {
            this.s = new Settings();
            d.dispose();
        }
        if (e.getActionCommand() == "valider") {
            FileReader fr = null;
            String motsauvegarde = "";
            String motupdate = "";
            int c = 0;
            final int ecart = 2;
            File f = null;
            try {
                f = new File("settings.txt");
                fr = new FileReader(f);
            } catch (FileNotFoundException exception) {
                System.out.print("Le fichier n'a pas été trouvé");
            }
            System.out.println("LA LECTURE COMMENCE ");
            while (c != -1) {
                if (c == 32) {
                    motsauvegarde = motsauvegarde.substring(0, motsauvegarde.length()-1);
                    if (motsauvegarde.equals("Height")) {
                        System.out.print("MATCH ok");
                        try {
                            //motupdate +=" ";
                            for (int i = 0; i < ecart; i++) {
                                c = fr.read();
                                motupdate += (char) c;
                            }
                            motupdate += (String) s.getSelectListH();
                           while ((char) c != '\n') {
                                c = fr.read();
                            }
                            motupdate+='\n';
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        motsauvegarde = "";
                    }
                    else if (motsauvegarde.equals("Width")) {
                        System.out.print("MATCH ok");
                        try {
                            //motupdate +=" ";
                            for (int i = 0; i < ecart; i++) {
                                c = fr.read();
                                motupdate += (char) c;
                            }
                            motupdate += (String) s.getSelectListW();
                            while ((char) c != '\n') {
                                c = fr.read();
                            }
                            motupdate+='\n';
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        motsauvegarde = "";
                    }
                    else if (motsauvegarde.equals("Map")) {
                        System.out.print("MATCH ok");
                        try {
                            //motupdate +=" ";
                            for (int i = 0; i < ecart; i++) {
                                c = fr.read();
                                motupdate += (char) c;
                            }
                            motupdate += (String) s.getSelectListMap();
                            while ((char) c != '\n') {
                                c = fr.read();
                            }
                            motupdate+='\n';
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        motsauvegarde = "";
                    }

                }
                try {
                    c = fr.read();
                    motupdate += (char) c;
                    motsauvegarde += (char) c;
                    System.out.print(c);
                    } catch (Exception e1) {

                    }
            }
            try {
                fr.close();
                FileWriter fw = new FileWriter(f);
                fw.write(motupdate.substring(0,motupdate.length()-1));
                fw.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }



    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        this.d=new Debut();
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}

//14h45
