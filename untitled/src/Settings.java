

// import com.intellij.ui.*;
// import com.intellij.ui.SingleSelectionModel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by willi on 09/03/2016.
 */

public class Settings extends JFrame {
    private JPanel selectionSize;
    private JPanel selectionMap;
    private Container cont;
    private JList listH;
    private JList listW;
    private JList listMap;
    public Settings() {
        this.setTitle("Settings");
        this.setSize(1200, 900);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        cont= this.getContentPane();
        cont.setLayout(null);
        cont.setBackground(Color.BLACK);

        selectionSize=new JPanel();
        selectionSize.setLayout(null);
        selectionSize.setBounds(100,100,400,300);
        selectionMap=new JPanel();
        selectionMap.setLayout(null);
        selectionMap.setBounds(510,100,400,300);




        // scrollPane.setPreferredSize(new Dimension(30,30));
        JLabel set=new JLabel("Settings");
        set.setBounds(160,0,120,40);
        set.setFont(new Font("gras", Font.BOLD, 21));

        JLabel map=new JLabel("Choix de map");
        map.setBounds(125,0,160,40);
        map.setFont(new Font("gras", Font.BOLD, 21));

        Vector<String> v = new Vector<String>();
        File repertoire = new File("./map");
        String[] children = repertoire.list();
        for(int i=0;i<children.length;i++){
            System.out.println(children[i]);
            v.add(children[i].substring(0,children[i].length()-4));
        }
        listMap = new JList(v);
        listMap.setVisibleRowCount(3);
        //list.setDragEnabled(false);
        // list.setPreferredSize(new Dimension(50,20));
        listMap.setFixedCellHeight(25);
        listMap.setFixedCellWidth(75);
        // list.setSelectionModel(new SingleSelectionModel());
        JScrollPane scrollPanemap = new JScrollPane(listMap);
        scrollPanemap.setBounds(150,60,75,50);
        scrollPanemap.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPanemap.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        JLabel width=new JLabel("Width");
        width.setBounds(75,40,50,20);
        width.setFont(new Font("gras",Font.BOLD,13));

        JLabel height=new JLabel("Height");
        height.setBounds(153,40,50,20);
        height.setFont(new Font("gras",Font.BOLD,13));

       // String[] size = {"440","660","924","1320"};
        String[] size = {"*0.5","*1","*1.5"};
        listH = new JList(size);
        listH.setVisibleRowCount(2);
        //list.setDragEnabled(false);
       // list.setPreferredSize(new Dimension(50,20));
        listH.setFixedCellHeight(25);
        listH.setFixedCellWidth(35);
       // list.setSelectionModel(new SingleSelectionModel());
        JScrollPane scrollPane = new JScrollPane(listH);
        scrollPane.setBounds(150,60,50,50);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

       // String[] size2 = {"380","570","798","1140"};
        String[] size2 = {"x0.5","x1","x1.5"};
        listW = new JList(size2);
        listW.setVisibleRowCount(2);
        //list.setDragEnabled(false);
        // list.setPreferredSize(new Dimension(50,20));
        listW.setFixedCellHeight(25);
        listW.setFixedCellWidth(35);
        // list.setSelectionModel(new SingleSelectionModel());
        JScrollPane scrollPane2 = new JScrollPane(listW);
        scrollPane2.setBounds(70,60,50,50);
        scrollPane2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        JButton valider = new JButton("Valider");
        valider.setBounds(150,150,100,20);
        //valider.setBounds(500,350,100,20);
        valider.setActionCommand("valider");

       // list.setAutoscrolls(true);
        //list.setPreferredSize(new Dimension(20,20));

        ControleurDebut o = new ControleurDebut(ControleurDebut.d,null,null,null);
        this.addWindowListener(o);
        valider.addActionListener(o);

        cont.add(selectionSize);
        cont.add(selectionMap);
        selectionMap.add(map);
        selectionMap.add(scrollPanemap);
        selectionSize.add(set);
        selectionSize.add(width);
        selectionSize.add(scrollPane);
        selectionSize.add(height);
        selectionSize.add(scrollPane2);
        selectionSize.add(valider);

        this.setVisible(true);
    }
    public Object getSelectListW(){
        return listW.getSelectedValue();
    }
    public Object getSelectListH(){
        return listH.getSelectedValue();
    }
    public Object getSelectListMap(){
        return listMap.getSelectedValue();
    }

}
