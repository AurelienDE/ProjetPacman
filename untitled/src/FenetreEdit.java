

//import com.intellij.ui.border.CustomLineBorder;
//import javafx.scene.layout.BorderImage;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by willi on 12/03/2016.
 */
public class FenetreEdit extends JFrame {
    JTextArea largeur;
    JTextArea hauteur;
    ControleurEditeur ce;
    JPanel paneVisual;
    JLabel labimage;
    JPanel paneOutils;
    Container cont;
    HashMap<Integer,JLabel> grille;
    HashMap<String,JLabel> bloclabel;
    ImageIcon blancimage;
    //CustomLineBorder clbselect;
    //CustomLineBorder clbunselect;
    JLabel erreur;
    public FenetreEdit(){
        bloclabel = new HashMap<>();
        grille = new HashMap<>();
        //clbselect = new CustomLineBorder(Color.RED,2,2,2,2);
        //clbunselect = new CustomLineBorder(Color.black,2,2,2,2);
        this.setTitle("EditeurMap");
        /*
            Fenetre Visualisation -> 750x750
            Fenetre Outils -> 750x300
         */
        this.setSize(1100,750);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        cont = this.getContentPane();
        cont.setLayout(null);
       // cont.setBounds(0,0,0,0);

        ce = new ControleurEditeur();
        /* Panel Visualisation de la grille */
        paneVisual=new JPanel(null);
        paneVisual.setBounds(0,0,750,750);

        /* Panel des outils */
        paneOutils= new JPanel(null);
        paneOutils.setBounds(750,0,350,750);

        /* Permet de faire apparaitre la grille */
        JLabel maxCaseH = new JLabel("MaxCaseHauteur");
        maxCaseH.setBounds(0,5,110,20);

        hauteur = new JTextArea();
        hauteur.setText("Ex: 20");
        hauteur.setBounds(110,5,40,20);
        hauteur.addFocusListener(ce);

        JLabel maxCaseW = new JLabel("MaxCaseLargeur");
        maxCaseW.setBounds(160,5,110,20);

        largeur = new JTextArea("Ex: 20");
        largeur.setBounds(270,5,40,20);
        largeur.addFocusListener(ce);

        JButton ValideCase = new JButton("Valider");
        ValideCase.setBounds(110,25,80,20);
        ValideCase.setActionCommand("ValideCase");
        ValideCase.addActionListener(ce);

        erreur=new JLabel("Veuillez entrer des nombres correctes");
        erreur.setBounds(40,43,250,20);
        erreur.setForeground(Color.red);
        erreur.setVisible(false);

        JButton Terminer = new JButton("Terminer");
        Terminer.setBounds(110,550,100,20);
        Terminer.setActionCommand("Terminer");
        Terminer.addActionListener(ce);

        /* Les bloc d'image dans panelOutils */
        int x = 30;
        int y = 65;
       // int y = 60;
        //int y = 60;
        for(String s : ce.em.bloc.keySet()){
            Image i = new ImageIcon(ce.em.bloc.get(s)).getImage().getScaledInstance(50,50,Image.SCALE_SMOOTH);
            //clbunselect = new CustomLineBorder(Color.black,2,2,2,2);
            ImageIcon ic = new ImageIcon(i);
            labimage = new JLabel(ic);
            //labimage.setFocusable(true);
            labimage.addMouseMotionListener(ce);
            labimage.addMouseListener(ce);
            labimage.setBounds(x,y,50,50);
            //labimage.setBorder(clbunselect);
            bloclabel.put(s,labimage);
            paneOutils.add(labimage);
            x+=80;
            if(x>300){
                x=30;
                y+=70;
            }
        }

        paneOutils.add(maxCaseH);
        paneOutils.add(hauteur);
        paneOutils.add(maxCaseW);
        paneOutils.add(largeur);
        paneOutils.add(ValideCase);
        paneOutils.add(erreur);
        paneOutils.add(Terminer);
        Image i2 = new ImageIcon("./image/blanc.png").getImage().getScaledInstance(30,30,Image.SCALE_SMOOTH);
        blancimage= new ImageIcon(i2);
        cont.add(paneVisual);
        cont.add(paneOutils);
        paneVisual.setVisible(false);
        this.setVisible(true);
    }
    public void AjouterGrille(){
        for(Case c : ce.em.g.getCaseLibre()){
            Image i2 = new ImageIcon("./image/blanc.png").getImage().getScaledInstance(29,29,Image.SCALE_SMOOTH);
            //CustomLineBorder clb = new CustomLineBorder(Color.black,1,1,1,1);
            ImageIcon ic2 = new ImageIcon(i2);
            JLabel labimagevide = new JLabel(ic2);
            //labimage.setFocusable(true);
            //labimagevide.addMouseMotionListener(ce);
            labimagevide.setBounds(c.getPosX(),c.getPosY(),29,29);
            //labimagevide.setBorder(clb);
            labimagevide.addMouseListener(ce);
            grille.put(c.id,labimagevide);
            paneVisual.add(labimagevide);
        }
        paneVisual.setOpaque(false);
        paneVisual.setVisible(true);
    }
}
