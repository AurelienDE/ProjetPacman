
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Pacman{
    private int posX;
    private int posY;
    private String nom;
    private Map<String,Image> images;
    private String imagePacman;
    private boolean mort;
    private int direction;
    private boolean disableDirection;
    int memoire;
    boolean affamer;
    boolean invincible;
    /*
        Y correspond a la largeur.
        X correspond a la hauteur.
     */
    public Pacman(int p1,int p2, String nom){
        this.affamer=false;
        this.posX=p1;
        this.posY=p2;
        this.invincible=false;
        this.nom=nom;
        this.mort=false;
        this.direction=0;
        this.memoire=0;
        this.disableDirection=false;
        this.images=new HashMap<String,Image>();
        File repertoire = new File("./image/Pacman");
        String[] children = repertoire.list();
        for(int i=0;i<children.length;i++){
           System.out.println(children[i]);
            try {
                BufferedImage image = ImageIO.read(new File("image/Pacman/"+children[i]));
                if(children[i].equals("PacmanDroiteO.png")){
                    this.imagePacman=children[i].substring(0,children[i].length()-4);
                }
                images.put(children[i].substring(0,children[i].length()-4),image);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public void redimImage(){
        for(String s :images.keySet()){
            Image tmp =images.get(s);
            Image image = tmp.getScaledInstance(ControleurDebut.f.getControl().getGrille().getSizeCase()+ControleurDebut.f.getControl().getGrille().getSizeCase()/3,
                    ControleurDebut.f.getControl().getGrille().getSizeCase()+ControleurDebut.f.getControl().getGrille().getSizeCase()/3,
                    Image.SCALE_SMOOTH);
            images.put(s,image);

        }
    }
    public boolean getMort(){
        return this.mort;
    }
    public void Mort(){
        this.mort= true;
    }
    public int getDirection(){
        return this.direction;
    }
    public void setDirection(int d){
        this.direction=d;
    }
    public void setPos(int x, int y){
        this.posX=x;
        this.posY=y;
    }
    public void incrementePosX(){
        posX+=(ControleurDebut.f.getControl().getGrille().getSizeCase())/2;
    }
    public void incrementePosY(){
        posY+=(ControleurDebut.f.getControl().getGrille().getSizeCase())/2;
    }
    public void decrementePosY(){
        posY-=(ControleurDebut.f.getControl().getGrille().getSizeCase())/2;
    }
    public void decrementePosX(){
        posX-=(ControleurDebut.f.getControl().getGrille().getSizeCase())/2;
    }
    public int getPosX(){
        return this.posX;
    }
    public int getPosY(){
        return this.posY;
    }
    public void setPosX(int pos) { this.posX=pos;}
    public void setPosY(int pos) { this.posY=pos;}
    public void setPosPacman(int posX,int posY){
        this.posX=posX;
        this.posY=posY;
    }
    public boolean getDisableDir(){
        return this.disableDirection;
    }
    public void setDisableDirection(boolean b){
        this.disableDirection=b;
    }
    public Image getImageActuel(){
        return this.images.get(this.imagePacman);
    }
    public void setStringImageActuel(String s){
        this.imagePacman=s;
    }
    public String getStringImageAcutel(){
        return this.imagePacman;
    }
    public String toString(){
        return "Pacman X : "+getPosX()+" Y : "+getPosY()+".";
    }
}