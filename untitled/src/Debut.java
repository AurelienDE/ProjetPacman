

import javax.swing.*;
import java.awt.*;

public class Debut extends JFrame {

    Container cont;
    Controleur c;
    MyButton joueurunjoueur;
    MyButton meilleurscore;

    public Debut() {
        this.setTitle("PACMAN");


       Dimension d=java.awt.Toolkit.getDefaultToolkit().getScreenSize();


        int hauteur=(int)(d.getHeight()/1.3);
        int largeur=(int)(d.getWidth()/2);
        System.out.print(hauteur);
        System.out.print(largeur);
       // this.setSize(630, 730);
       this.setSize(largeur,hauteur);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setBackground(Color.RED);

        ControleurDebut o = new ControleurDebut(this,null,null,null);
        cont = this.getContentPane();

        JPanel panel= new JPanel();

        panel.setLayout (new BoxLayout( panel , BoxLayout.Y_AXIS ) ) ;

     //   ImageIcon backgroundRaw = new ImageIcon("image/pacman_layout.JPG");
        ImageIcon background = new ImageIcon(new ImageIcon("image/Accueil/imageacceuil.png").getImage());
        JLabel labBackground= new JLabel(background);
        labBackground.setAlignmentX(Component.CENTER_ALIGNMENT);

        panel.add(labBackground);

        joueurunjoueur = new MyButton("Jouer 1 joueur","image/btn.png","");
        MyButton joueurdeuxjoueur = new MyButton("Jouer 2 joueur","image/btn.png","imge/fondblanc.png");
        MyButton Settings = new MyButton("Seetings","image/btn.png","image/fondblanc.png");
        MyButton aide = new MyButton("Aide","image/btn.png","imae/fondblanc.png");
        MyButton meilleurscore = new MyButton("Meilleur score","image/btn.png","image/fondblanc.png");
        MyButton Editeur = new MyButton("Editeur","image/btn.png","image/fondblanc.png");
        meilleurscore.setActionCommand("lienmeilleurscore");
        meilleurscore.addActionListener(o);

        JPanel joueurunjoueurpanel= new JPanel();
        JPanel joueurdeuxjoueurpanel= new JPanel();
        JPanel aidepanel= new JPanel();
        JPanel meilleurscorepanel= new JPanel();
        JPanel Settingspanel = new JPanel();
        JPanel Editeurpanel = new JPanel();


        joueurunjoueur.setPreferredSize(new Dimension(200, 35));
        joueurdeuxjoueur.setPreferredSize(new Dimension(200, 35));
        aide.setPreferredSize(new Dimension(200,35));
        meilleurscore.setPreferredSize(new Dimension(200, 35));
        Settings.setPreferredSize(new Dimension(200,35));
        Editeur.setPreferredSize(new Dimension(200,35));

        joueurunjoueurpanel.setBackground( Color.black );
        joueurdeuxjoueurpanel.setBackground( Color.black );
        aidepanel.setBackground( Color.black );
        meilleurscorepanel.setBackground( Color.black );
        Settingspanel.setBackground(Color.BLACK);
        Editeurpanel.setBackground(Color.black);

        //~ /* ActionBouton o = new ActionBouton (fenetre,this,null,null,null,null,null); */


        joueurunjoueur.setActionCommand("lienjouerunjoueur");
        joueurunjoueur.addActionListener(o);

        Settings.setActionCommand("settings");
        Settings.addActionListener(o);

        Editeur.setActionCommand("editeur");
        Editeur.addActionListener(o);

        joueurunjoueurpanel.add(joueurunjoueur);
        joueurdeuxjoueurpanel.add(joueurdeuxjoueur);
        aidepanel.add(aide);
        meilleurscorepanel.add(meilleurscore);
        Settingspanel.add(Settings);
        Editeurpanel.add(Editeur);


        panel.add(joueurunjoueurpanel);
        panel.add(joueurdeuxjoueurpanel);
        panel.add(Settingspanel);
        panel.add(aidepanel);
        panel.add(meilleurscorepanel);
        panel.add(Editeurpanel);

        panel.setBackground(Color.black);

        cont.add(panel);
        cont.setBackground(Color.black);
        this.setVisible(true);

    }

}
