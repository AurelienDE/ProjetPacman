
import java.util.ArrayList;


public class Grille {
    int hauteur;
    int largeur;
    int maxCaseH;
    int maxCaseL;
    /* non final car resolution peut changer */
    private static int sizecase=30;
    private static int sizecasedemi;
    private ArrayList<Case> cases; // ensembles des cases du jeu
    private ArrayList<Case> caseOccuper; //Ensemble des cases murs
    int pointVie=3;
    private static int scoretotal;

    private ArrayList<Case> caseLibre; // Ensemble des cases ???

    ArrayList<Case> caseLibreNonboulejaune;

    ArrayList<Case> casebouleinvincible; // Ensemble des cases invincible
    ArrayList<Case> casecerise; // ??? Normalement la cerise elle apparait toujours au meme endroit.
    ArrayList<Case> casePorte;
    Case casebegin;

    /*
        X correspond a la largeur.
        Y correspond a la hauteur.
     */
    /*
        Construit une grille définie par une hauteur, une largeur, la hauteurMax d'une case,
        la largeurMax calculé par la taille globale de la case.
        La grille possede differents tableaux de case.
    */
    public Grille(int y, int x) {
        this.hauteur = y;
        this.largeur = x;
        this.maxCaseH=hauteur/sizecase;
        this.maxCaseL=largeur/sizecase;


       // System.out.println(maxCaseH);
       // System.out.println(maxCaseL);
        casecerise=new ArrayList<Case>();

        cases=new ArrayList<Case>();
        caseOccuper=new ArrayList<Case>();
        caseLibre=new ArrayList<Case>();
        caseLibreNonboulejaune=new ArrayList<Case>();
        casebouleinvincible=new ArrayList<Case>();
        casePorte=new ArrayList<Case>();

    }
    /*
     Permet de redefinir une grille apres un LoadMap par exemple.
    */
    public void redefineGrille(int x, int y){
        this.hauteur = x;
        this.largeur = y;
        this.maxCaseH=hauteur/sizecase;
        this.maxCaseL=largeur/sizecase;

    }
    public void redefineGrille2(int x,int y){
        this.hauteur = x;
        this.largeur = y;
        System.out.println("===============");
        System.out.println(hauteur);
        System.out.println(largeur);
        System.out.println(maxCaseH);
        System.out.println(maxCaseL);
        System.out.println(sizecase);
    }
    /*
        La grille est chargé par la classe LoadMap.
     */
    public void setGrille(){
        new LoadMap(this);
        scoretotal=casebouleinvincible.size()*50+caseLibre.size()*10;
        System.out.println("SCORE TOTAL : "+scoretotal);
    }
    public void setGrille(int niveau){
        if(niveau==0){
            this.setGrille();
        }
        else {
            new LoadMap(this, niveau);
        }
        scoretotal=casebouleinvincible.size()*50+caseLibre.size()*10;
        System.out.println("SCORE TOTAL : "+scoretotal);
    }

    /*
        Permet de connaitre la case sur laquel ce trouve le pacman.
        En cas de doute, sur la case du pacman,le changement de direction du pacman est désactivé.
        Le doute arrive lorsque le pacman est entre deux cases.
     */

    public Case getCasePacman(){
        int total=0;
        Pacman pc=ControleurDebut.f.getControl().getPacman();
        int i=pc.getPosY()/sizecase;
        if(pc.getPosY()%sizecase!=0){
            pc.setDisableDirection(true);
        }
        total+=i*maxCaseH;
        int i2=pc.getPosX()/sizecase;
        if(pc.getPosX()%sizecase!=0){
            pc.setDisableDirection(true);
        }
        total+=i2;
        return cases.get(total);
    }


    public Case getCaseFantome(Ghost g){
        //  System.out.println("Case Pacman");
        int total=0;
        int i=g.getPosY()/sizecase;
        if(g.getPosY()%sizecase!=0){
            g.setDisableDirection(true);
            // pc.memoire=pc.getDirection();
        }
        total+=i*maxCaseH;
        int i2=g.getPosX()/sizecase;
        if(g.getPosX()%sizecase!=0){
            g.setDisableDirection(true);
            //  pc.memoire=pc.getDirection();
        }
        total+=i2;
        return cases.get(total);
    }

    public int getCasePacmanX1(){
        int total=0;
        Pacman pc=ControleurDebut.f.getControl().getPacman();
        int i=pc.getPosX()/sizecase;
        return i;
    }

    public int getCasePacmanY1(){

        Pacman pc=ControleurDebut.f.getControl().getPacman();
        int i2=pc.getPosY()/sizecase;
        return i2;
    }


    /*
        Les differentes fonctions suivantes permettent de connaitre les cases ce trouvant autour d'une
        case passer en parametre.
     */

    public Case getCaseDroite(Case c){
        int total=0;
        //Pacman pc=main.f.getControl().getPacman();
        int i=c.getPosY()/sizecase;
        total+=i*maxCaseH+1;
        int i2=c.getPosX()/sizecase;
        total+=i2;
        return cases.get(total);
    }
    public Case getCaseGauche(Case c){
        int total=0;
        int i=c.getPosY()/sizecase;
        total+=i*maxCaseH-1;
        int i2=c.getPosX()/sizecase;
        total+=i2;
        if(total<0){
            total=0;
        }
        return cases.get(total);
    }

    public Case getCaseBas(Case c){
        int total=0;
        int i=c.getPosY()/sizecase;
        total+=i*maxCaseH;
        int i2=c.getPosX()/sizecase;
        total+=i2;
        total+=maxCaseH;
        return cases.get(total);
    }
    public Case getCaseHaut(Case c){
        int total=0;
        int i=c.getPosY()/sizecase;
        total+=i*maxCaseH;
        int i2=c.getPosX()/sizecase;
        total+=i2;
        total-=maxCaseH;

        //System.out.println("Case de Bas"+cases.get(total));
        // System.out.println("Case de Bas"+cases.get(total).getType());

        return cases.get(total);
    }
    /* ¨
    Raccourci pour acceder au Pacman.
     */
    public Pacman getPacman(){
        return ControleurDebut.f.getControl().getPacman();
    }
    /*
        Les differentes fonctions suivantes permettent de savoir si il est possible de
        se deplacer sur une case adjacentes pour le pacman.
        Afin que cela fonctionne pour les fantomes egalement il suffit de rajouter un parametre : Objet.
     */
    public boolean DeplacementDroiteOK(Case pacman){
        if(this.getCaseDroite(pacman).DeplacementPossiblePacman()){
            return true;
        }
        return false;
    }
    public boolean DeplacementGaucheOK(Case pacman){
        if(this.getCaseGauche(pacman).DeplacementPossiblePacman()){


           // System.out.println(this.getPacman().getPosX());
           // System.out.println(this.getPacman().getPosY());
           // if(this.getCaseGauche(tmp).DeplacementPossible()) {
                //System.out.println("CAS A");
                return true;
      //   }


        }
        else if((this.getPacman().getPosX())-(sizecase/2)>=this.getCasePacman().getPosX()){
            return true;
        }
        return false;
    }
    public boolean DeplacementHautOK(Case pacman){
        if(this.getCaseHaut(pacman).DeplacementPossiblePacman()){
            return true;
        }
        else if((this.getPacman().getPosY())-(sizecase/2)>=this.getCasePacman().getPosY()){
            return true;
        }
        return false;
    }
    public boolean DeplacementBasOK(Case pacman){
        if(this.getCaseBas(pacman).DeplacementPossiblePacman()){
            return true;
        }
        return false;
    }




    public Ghost getFantomeun(){
        return ControleurDebut.f.getControl().getGhostun();
    }
    public Ghost getFantomedeux(){
        return ControleurDebut.f.getControl().getGhostdeux();
    }
    public Ghost getFantometrois(){
        return ControleurDebut.f.getControl().getGhosttrois();
    }
    public Ghost getFantomequatre(){
        return ControleurDebut.f.getControl().getGhostquatre();
    }

    //DEPLACEMENT FANTOME1


    public boolean DeplacementDroiteFantomeOKun(Case pacman){
        if(this.getCaseDroite(pacman).DeplacementPossibleFantomeun()){
            return true;
        }
        return false;
    }
    public boolean DeplacementGaucheFantomeOKun(Case pacman){
        if(this.getCaseGauche(pacman).DeplacementPossibleFantomeun()){

            // System.out.println(this.getPacman().getPosX());
            // System.out.println(this.getPacman().getPosY());
            // if(this.getCaseGauche(tmp).DeplacementPossible()) {
            //System.out.println("CAS A");
            return true;
            //   }

        }
        else if((this.getFantomeun().getPosX())-(sizecase/2)>=this.getCaseFantome(getFantomeun()).getPosX()){
            return true;
        }
        return false;
    }
    public boolean DeplacementHautFantomeOKun(Case pacman){
        if(this.getCaseHaut(pacman).DeplacementPossibleFantomeun()){
            return true;
        }
        else if((this.getFantomeun().getPosY())-(sizecase/2)>=this.getCaseFantome(getFantomeun()).getPosY()){
            return true;
        }
        return false;
    }
    public boolean DeplacementBasFantomeOKun(Case pacman){
        if(this.getCaseBas(pacman).DeplacementPossibleFantomeun()){
            return true;
        }
        return false;
    }

    // Fantome 2

    public boolean DeplacementDroiteFantomeOKdeux(Case pacman){
        if(this.getCaseDroite(pacman).DeplacementPossibleFantomedeux()){
            return true;
        }
        return false;
    }
    public boolean DeplacementGaucheFantomeOKdeux(Case pacman){
        if(this.getCaseGauche(pacman).DeplacementPossibleFantomedeux()){

            // System.out.println(this.getPacman().getPosX());
            // System.out.println(this.getPacman().getPosY());
            // if(this.getCaseGauche(tmp).DeplacementPossible()) {
            //System.out.println("CAS A");
            return true;
            //   }

        }
        else if((this.getFantomedeux().getPosX())-(sizecase/2)>=this.getCaseFantome(getFantomedeux()).getPosX()){
            return true;
        }
        return false;
    }
    public boolean DeplacementHautFantomeOKdeux(Case pacman){
        if(this.getCaseHaut(pacman).DeplacementPossibleFantomedeux()){
            return true;
        }
        else if((this.getFantomedeux().getPosY())-(sizecase/2)>=this.getCaseFantome(getFantomedeux()).getPosY()){
            return true;
        }
        return false;
    }
    public boolean DeplacementBasFantomeOKdeux(Case pacman){
        if(this.getCaseBas(pacman).DeplacementPossibleFantomedeux()){
            return true;
        }
        return false;
    }


    // Fantome 2

    public boolean DeplacementDroiteFantomeOKtrois(Case pacman){
        if(this.getCaseDroite(pacman).DeplacementPossibleFantometrois()){
            return true;
        }
        return false;
    }
    public boolean DeplacementGaucheFantomeOKtrois(Case pacman){
        if(this.getCaseGauche(pacman).DeplacementPossibleFantometrois()){

            // System.out.println(this.getPacman().getPosX());
            // System.out.println(this.getPacman().getPosY());
            // if(this.getCaseGauche(tmp).DeplacementPossible()) {
            //System.out.println("CAS A");
            return true;
            //   }

        }
        else if((this.getFantometrois().getPosX())-(sizecase/2)>=this.getCaseFantome(getFantometrois()).getPosX()){
            return true;
        }
        return false;
    }
    public boolean DeplacementHautFantomeOKtrois(Case pacman){
        if(this.getCaseHaut(pacman).DeplacementPossibleFantometrois()){
            return true;
        }
        else if((this.getFantometrois().getPosY())-(sizecase/2)>=this.getCaseFantome(getFantometrois()).getPosY()){
            return true;
        }
        return false;
    }
    public boolean DeplacementBasFantomeOKtrois(Case pacman){
        if(this.getCaseBas(pacman).DeplacementPossibleFantometrois()){
            return true;
        }
        return false;
    }


    // Fantome 2

    public boolean DeplacementDroiteFantomeOKquatre(Case pacman){
        if(this.getCaseDroite(pacman).DeplacementPossibleFantomequatre()){
            return true;
        }
        return false;
    }
    public boolean DeplacementGaucheFantomeOKquatre(Case pacman){
        if(this.getCaseGauche(pacman).DeplacementPossibleFantomequatre()){

            // System.out.println(this.getPacman().getPosX());
            // System.out.println(this.getPacman().getPosY());
            // if(this.getCaseGauche(tmp).DeplacementPossible()) {
            //System.out.println("CAS A");
            return true;
            //   }

        }
        else if((this.getFantomequatre().getPosX())-(sizecase/2)>=this.getCaseFantome(getFantomequatre()).getPosX()){
            return true;
        }
        return false;
    }
    public boolean DeplacementHautFantomeOKquatre(Case pacman){
        if(this.getCaseHaut(pacman).DeplacementPossibleFantomequatre()){
            return true;
        }
        else if((this.getFantomequatre().getPosY())-(sizecase/2)>=this.getCaseFantome(getFantomequatre()).getPosY()){
            return true;
        }
        return false;
    }
    public boolean DeplacementBasFantomeOKquatre(Case pacman){
        if(this.getCaseBas(pacman).DeplacementPossibleFantomequatre()){
            return true;
        }
        return false;
    }



    /*
        getDirection() : Donne la direction d'ou vient le pacman.
     */

    public int getDirection(Case pacman){
        int compteur=0;
        int resultat= getPacman().getDirection();
        int resSave= getPacman().getDirection();
        int position=20;
        if(resultat==0 || resultat==2){
            position=4;
        }
        else if(resultat==4){
            position=2;
        }
        else if(resultat==1){
            position=3;
        }
        else if(resultat==3){
            position=1;
        }
//        if (getCaseDroite(pacman).DeplacementPossible() && position!=2) {
//            compteur+=1;
//            resultat=2;
//        }
//        if (getCaseHaut(pacman).DeplacementPossible() && position!=1) {
//            compteur+=1;
//            resultat=1;
//        }
//        if (getCaseGauche(pacman).DeplacementPossible() && position!=4) {
//            compteur+=1;
//            resultat=4;
//        }
//        if (getCaseBas(pacman).DeplacementPossible() && position!=3) {
//            compteur+=1;
//            resultat=3;
//        }
//        //System.out.println(compteur);
//        if(compteur==1){
//            return resultat;
//        }
//        else{
//            return resSave;
//        }
        return resultat;
    }
    public void setSizeCase(int s){
        sizecase=s;
    }
    public int getSizeCase(){
        return sizecase;
    }
    public ArrayList<Case> getCaseOccuper(){
        return this.caseOccuper;
    }

    public ArrayList<Case> getCase() {
        return this.cases;
    }
    public Case getC(int id){
        for(Case c : this.cases){
            if(c.id==id){
                return c;
            }
        }
        return null;
    }
    public ArrayList<Case> getCaseLibre(){
        return this.caseLibre;

    }
}