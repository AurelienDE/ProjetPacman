

import javax.sound.sampled.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by willi on 03/03/2016.
 */
public class Game {
    Map<String,Clip> musique;
    Map<String,AudioInputStream> audio;
    String nom;
    static int niveau = 0;
    private boolean start=false;

    public Game(String s){
        this.nom = s;
        musique = new HashMap<String,Clip>();
        audio = new HashMap<String,AudioInputStream>();

    }

    public void addMusique(String chemin, String titre) throws IOException, UnsupportedAudioFileException, LineUnavailableException {
        URL url = this.getClass().getClassLoader().getResource(chemin);
        AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
        // Get a sound clip resource.
        Clip c;
        c = AudioSystem.getClip();
        c.open(audioIn);
        System.out.println("titre");
        musique.put(titre,c);
        audio.put(titre,audioIn);
    }
    public void PlayMusique(String titre) throws IOException, LineUnavailableException {
        if(!musique.get(titre).isOpen()){
            musique.get(titre).open(audio.get(titre));
        }
        musique.get(titre).start();
    }
    public Clip getMusique(String titre){
        return this.musique.get(titre);
    }
    public void StopMusique(String titre){
        musique.get(titre).stop();
    }

    public boolean musiqueEnCours(String titre){
        return musique.get(titre).isRunning();
    }
    public boolean getStart(){
        return this.start;
    }
    public void setStart(boolean b){
        this.start=b;
    }
}
