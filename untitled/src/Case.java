
/**
 * Created by willi on 04/02/2016.
 */
public class Case {
    static int uid=0;
    int id;
    private int posX1,posX2,posY1,posY2;
    private int type;
    /*
        type :
            0 = Libre avec boule
            1 = Cases mur
            2 = Teleporteur
            3 = PorteFantome
            //4 = Fantome
            5 = BouleInvincible
     */
    private boolean libre;

    public Case(int a,int b, int c, int d,int type){
        id=uid;
        uid++;
        this.posX1=a;
        this.posX2=b;
        this.posY1=c;
        this.posY2=d;
        this.type=type;
        if(this.type==0 || this.type==5 || this.type==10){
            this.libre=true;
        }
        else{
            this.libre=false;
        }
    }
    public int getType(){
        return this.type;
    }
    // bizarre marche pour droite et bas //
    public boolean DeplacementPossiblePacman(){

        if(this.type==0 || this.type==2 || this.type==5 || this.type==10){
            return true;
        }
        else{
            return false;
        }
    }
    public boolean DeplacementPossibleFantomeun(){
        if(this.type==3 && ControleurDebut.f.getControl().getGhostun().sortie==false){
            ControleurDebut.f.getControl().getGhostun().sortie =true;
            return true;
        }
        if(this.type==0 || this.type==2 || this.type==5 || this.type==10){

            return true;
        }
        else{
            return false;
        }
    }

    public boolean DeplacementPossibleFantomedeux(){
        if(this.type==3 && ControleurDebut.f.getControl().getGhostdeux().sortie==false){
            ControleurDebut.f.getControl().getGhostdeux().sortie =true;
            return true;
        }
        if(this.type==0 || this.type==2 || this.type==5 || this.type==10){

            return true;
        }
        else{
            return false;
        }
    }

    public boolean DeplacementPossibleFantometrois(){
        if(this.type==3 && ControleurDebut.f.getControl().getGhosttrois().sortie==false){
            ControleurDebut.f.getControl().getGhosttrois().sortie =true;
            return true;
        }
        if(this.type==0 || this.type==2 || this.type==5 || this.type==10){

            return true;
        }
        else{
            return false;
        }
    }


    public boolean DeplacementPossibleFantomequatre(){
        if(this.type==3 && ControleurDebut.f.getControl().getGhostquatre().sortie==false){
            ControleurDebut.f.getControl().getGhostquatre().sortie =true;
            return true;
        }
        if(this.type==0 || this.type==2 || this.type==5 || this.type==10){

            return true;
        }
        else{
            return false;
        }
    }


    public void setType(int num){
        this.type=num;
    }

    public Pacman getPacman(){
        return ControleurDebut.f.getControl().getPacman();
    }


    public String toString(){
          return "Case X : "+posX1+","+posX2+" Case Y : "+posY1+","+posY2+".";
      }
    public int getPosX(){
        return this.posX1;
    }
    public int getPosX2(){
        return this.posX2;
    }
    public int getPosY(){
        return this.posY1;
    }

    public int getPosY2(){return this.posY2 ;}

}
