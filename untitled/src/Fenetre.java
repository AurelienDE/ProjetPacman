

import javax.imageio.ImageIO;
import javax.swing.*;

import javax.swing.JPanel;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.awt.Graphics;
import java.io.IOException;
import java.util.ArrayList;

public class Fenetre extends JFrame{
    public BufferedImage boulejaune;
    public BufferedImage impacman;
    public BufferedImage boulejaunenoir;
    public BufferedImage cerise;

    public BufferedImage imageghostpeureu;
    public BufferedImage imageghostmange;
    public BufferedImage  pacmanbase;
    public BufferedImage imageghostun;
    public BufferedImage imageghostdeux;
    public BufferedImage imageghosttrois;
    public BufferedImage imageghostquatre;

    public BufferedImage bouleinvincibleimage;

    public BufferedImage image;
    public BufferedImage image2;
    public BufferedImage background;

    JPanel test;
    JPanel bouleinvinciblepanel;

    ArrayList listeboulejauneun;
    ArrayList listeboulejaunedeux;


    int compteurscore=0;
    int compteurvie=3;

    boolean start=false;

    boolean swap=false;
    boolean pause=false;
    JPanel paneprincpal;
    JPanel paneGrille;
    JLabel labPacman;
    JLabel affscore;
    Container cont;
    Controleur c;


    ImageIcon ghostun;
    JLabel labelghostun;
    ImageIcon ghostdeux;
    JLabel labelghostdeux;
    ImageIcon ghosttrois;
    JLabel labelghosttrois;

    ImageIcon ghostquatre;
    JLabel labelghostquatre;




    ImageIcon pacman;
    JLabel labBackground;
    ImageIcon gomme;
    JLabel labGomme;
    JLabel ready;
    JLabel affvie;
    JLabel lavie;
    JLabel laviedeux;
    JLabel lavietrois;


     JPanel score;

    JPanel nombreviespanel;
    JPanel testcerise;

    ImageIcon ceriseimage;
    JLabel labcerise;

    // private Image photo1 = getToolkit().getImage("pacman1.png");

    public Fenetre(int ... Score) throws IOException {
        this.setTitle("PACMAN");
        try {
            compteurscore=Score[0];
            compteurvie=Score[1];
        }
        catch(Exception e){

        }
        c = new Controleur();
        c.gr.pointVie=compteurvie;
        this.setSize(getControl().getGrille().hauteur+230,getControl().getGrille().largeur+40);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        try {

            imageghostpeureu=ImageIO.read(new File("image/fantomepeureu.png"));
            imageghostmange=ImageIO.read(new File("image/fantomemange.png"));
            cerise= ImageIO.read(new File("image/cerise.png"));
            bouleinvincibleimage= ImageIO.read(new File("image/bouleinvincible.png"));

            boulejaunenoir = ImageIO.read(new File("image/noir.png"));
          //  image = ImageIO.read(new File("image/pacman42.png"));
           // image2 = ImageIO.read(new File("image/pacman43.png"));
            background = ImageIO.read(new File("image/pacman_layout.JPG"));
            imageghostun = ImageIO.read(new File("image/fantomerouge.png"));
            imageghostdeux = ImageIO.read(new File("image/fantomeorange.png"));
            imageghosttrois = ImageIO.read(new File("image/fantomebleu.png"));
            imageghostquatre = ImageIO.read(new File("image/fantomeviolet.png"));

        } catch (Exception e) {
            e.printStackTrace();
        }

        score = new JPanel();
        score.setLayout(null);
        score.setBounds(0, 0, 1200, 1800);
        score.setPreferredSize(new Dimension(1200, 1800));
        score.setOpaque(false);
        score.setDoubleBuffered(true);


        affvie=new JLabel("<html><body><p> <font color='white'>Point de vie : "+compteurvie+"</font></p></body></html>");
        //affvie.setBounds(700, 100, 200, 100);
        affvie.setBounds(getControl().getGrille().hauteur+50, 90, 150, 100);
        affvie.setPreferredSize(new Dimension(200, 100));

        nombreviespanel = new JPanel();
        ImageIcon unevie = new ImageIcon(new ImageIcon("image/Pacman/PacmanDroiteO.png").getImage());
         lavie = new JLabel(unevie);
         laviedeux = new JLabel(unevie);
         lavietrois = new JLabel(unevie);
       // lavie.setBounds(700, 300, 400, 400);
        //lavie.setPreferredSize(new Dimension(400, 400));
        if(compteurvie==3) {
            nombreviespanel.add(lavie);
            nombreviespanel.add(laviedeux);
            nombreviespanel.add(lavietrois);
        }
        else if(compteurvie==2){
            nombreviespanel.add(lavie);
            nombreviespanel.add(laviedeux);
        }
        else{
            nombreviespanel.add(lavie);
        }

       // nombreviespanel.setBounds(700, 170, 150, 100);
        nombreviespanel.setBounds(getControl().getGrille().hauteur+5, 150, 200, 100);
        nombreviespanel.setPreferredSize(new Dimension(150, 100));
        nombreviespanel.setBackground(Color.black);
        System.out.println("VIE :"+c.gr.pointVie);

        affscore=new JLabel("<html><body><p> <font color='white'> Score :"+compteurscore+"</font></p></body></html>");
        affscore.setBounds(getControl().getGrille().hauteur+50, 50, 100, 100);
        affscore.setPreferredSize(new Dimension(100, 100));

        Font font = new Font("FreeMono",Font.BOLD, 15);
        affvie.setFont(font);
        affscore.setFont(font);

        score.add(affscore);
        score.add(affvie);
        score.add(nombreviespanel);

        cont = this.getContentPane();
        cont.setLayout(null);
        cont.setBounds(0, 0, getControl().getGrille().hauteur+230, getControl().getGrille().largeur+40);

        ready=new JLabel("Get ready ? Press s fort start !");
        ready.setOpaque(true);
        ready.setBackground(Color.DARK_GRAY);
        ready.setFont(new Font(null,Font.BOLD,getControl().getGrille().getSizeCase()-10));
        ready.setForeground(Color.RED);
        ready.setBounds(getControl().getGrille().getSizeCase()*6, getControl().getGrille().getSizeCase()*6, getControl().getGrille().getSizeCase()*8, 18);

        System.out.println(getControl().pc.getImageActuel());
        pacman = new ImageIcon(getControl().pc.getImageActuel());
        labPacman = new JLabel(pacman);
        System.out.println(getControl().getGrille().getSizeCase());
        System.out.println(c.getPacman().getPosX());
        labPacman.setBounds(c.getPacman().getPosX(), c.getPacman().getPosY(), getControl().getGrille().getSizeCase(), getControl().getGrille().getSizeCase());

        ghostun = new ImageIcon("image/fantomerouge.png");
        labelghostun = new JLabel(ghostun);
        labelghostun.setBackground(Color.black);
        labelghostun.setBounds(c.getGhostun().getPosX(), c.getGhostun().getPosY(), getControl().getGrille().getSizeCase(), getControl().getGrille().getSizeCase());


        ghostdeux = new ImageIcon("image/fantomeorange.png");
        labelghostdeux = new JLabel(ghostdeux);
        labelghostdeux.setBackground(Color.black);
        labelghostdeux.setBounds(c.getGhostdeux().getPosX(), c.getGhostdeux().getPosY(), 30, 30);

        ghosttrois = new ImageIcon("image/fantomebleu.png");
        labelghosttrois = new JLabel(ghosttrois);
        labelghosttrois.setBackground(Color.black);
        labelghosttrois.setBounds(c.getGhosttrois().getPosX(), c.getGhosttrois().getPosY(), 30, 30);

        ghostquatre = new ImageIcon("image/fantomeviolet.png");
        labelghostquatre = new JLabel(ghostquatre);
        labelghostquatre.setBackground(Color.black);
        labelghostquatre.setBounds(c.getGhostquatre().getPosX(), c.getGhostquatre().getPosY(), 30, 30);

        ImageIcon backgroundRaw = new ImageIcon("image/pacman_layout.JPG");
        ImageIcon background = new ImageIcon(new ImageIcon("image/pacman_layout.JPG").getImage().getScaledInstance(1100, 750, Image.SCALE_SMOOTH));
        JLabel labBackground = new JLabel(background);

        // labBackground.setBounds(0,0,1100,750);

        paneprincpal = new JPanel();
        paneprincpal.add(ready);

        //labBackground= new JLabel(background);
       // labBackground.setBounds(0,0,1100,750);

        paneprincpal.setLayout(null);
        paneprincpal.setBounds(0, 0, getControl().getGrille().hauteur+230, getControl().getGrille().largeur+40);
        paneprincpal.setPreferredSize(new Dimension(getControl().getGrille().hauteur+230, getControl().getGrille().largeur+40));
        paneprincpal.setOpaque(false);
        paneprincpal.setDoubleBuffered(true);
        paneprincpal.setBackground(Color.WHITE);

        paneGrille = new JPanel();
        paneGrille.setLayout(null);
        paneGrille.setBounds(0, 0, getControl().getGrille().hauteur+230, getControl().getGrille().largeur+40);
        paneGrille.setPreferredSize(new Dimension(getControl().getGrille().largeur, getControl().getGrille().hauteur));
        paneGrille.setOpaque(false);
        paneGrille.setDoubleBuffered(true);
        paneGrille.setBackground(Color.WHITE);

        paneGrille.add(labPacman);
        paneGrille.add(labelghostun);
        paneGrille.add(labelghostdeux);
        paneGrille.add(labelghosttrois);
        paneGrille.add(labelghostquatre);
        test = new JPanel();
         test.setLayout(null);
         test.setBounds(0, 0, 1200, 1800);
         test.setPreferredSize(new Dimension(1200, 1800));
         test.setOpaque(false);
         test.setDoubleBuffered(true);
         test.setBackground(Color.black);
        paneGrille.setBackground(Color.black);
        paneprincpal.setBackground(Color.black);
        cont.setBackground(Color.black);
// =======
        boolean casedroite=false;
        boolean casegauche=false;
        boolean casehaut=false;
        boolean casebas=false;
        int dimension = getControl().getGrille().getSizeCase();
        for (Case cas : this.getControl().getGrille().getCaseOccuper()) {
            casedroite=false;
            casehaut=false;
            casebas=false;
            casegauche=false;
            try{
                if(cas.getPosX()==getControl().getGrille().hauteur-getControl().getGrille().getSizeCase()){
                    casedroite=false;
                }
                else if(getControl().gr.getCaseDroite(cas).getType()==1){
                    casedroite=true;
                    //System.out.println("SALUUUUT"+getControl().gr.getCase().get(398));
                    //System.out.println("SALUUUUT"+getControl().gr.getCase().get(398).getType());
                }

            }
            catch(Exception e){
                casedroite=false;
            }
            try {
                if (getControl().gr.getCaseHaut(cas).getType() == 1) {
                    casehaut = true;
                }
            }
            catch(Exception e){
                casehaut=false;
            }
            try {
                if(cas.getPosX()==0){
                    casegauche=false;
                }
                else if (getControl().gr.getCaseGauche(cas).getType() == 1) {
                    casegauche = true;
                }
            }
            catch(Exception e){
                casegauche=false;
            }
            try {
                if (getControl().gr.getCaseBas(cas).getType() == 1) {
                    casebas = true;
                }
            }
            catch(Exception e){
                casebas=false;
            }
            if(casedroite && casehaut && casegauche && casebas){
                /* Je dessine un mur noir */
                ImageIcon mur = new ImageIcon(new ImageIcon("image/pacman_layout.JPG").getImage().getScaledInstance(dimension, dimension, Image.SCALE_SMOOTH));
                JLabel labmur = new JLabel(mur);
                labmur.setBounds(cas.getPosX(), cas.getPosY(),dimension , dimension);
                paneprincpal.add(labmur);
            }
            else if(casedroite && casehaut && casegauche){
                /* Je dessine un mur noir avec un coté bas */
                ImageIcon mur = new ImageIcon(new ImageIcon("image/Mur/MurBas.png").getImage().getScaledInstance(dimension, dimension, Image.SCALE_SMOOTH));
                JLabel labmur = new JLabel(mur);
                labmur.setBounds(cas.getPosX(), cas.getPosY(),dimension , dimension);
                paneprincpal.add(labmur);
            }
            else if(casedroite && casehaut && casebas){
                /* je dessine un mur noir avec un coté gauche */
                ImageIcon mur = new ImageIcon(new ImageIcon("image/Mur/MurGauche.png").getImage().getScaledInstance(dimension, dimension, Image.SCALE_SMOOTH));
                JLabel labmur = new JLabel(mur);
                labmur.setBounds(cas.getPosX(), cas.getPosY(),dimension , dimension);
                paneprincpal.add(labmur);
            }
            else if(casedroite && casebas && casegauche){
                /* je dessine un mur noir avec un coté haut */
                ImageIcon mur = new ImageIcon(new ImageIcon("image/Mur/MurHaut.png").getImage().getScaledInstance(dimension, dimension, Image.SCALE_SMOOTH));
                JLabel labmur = new JLabel(mur);
                labmur.setBounds(cas.getPosX(), cas.getPosY(),dimension , dimension);
                paneprincpal.add(labmur);
            }
            else if(casegauche && casebas && casehaut){
                /* Je dessine un moir noir avec un coté droit */
                ImageIcon mur = new ImageIcon(new ImageIcon("image/Mur/MurDroite.png").getImage().getScaledInstance(dimension, dimension, Image.SCALE_SMOOTH));
                JLabel labmur = new JLabel(mur);
                labmur.setBounds(cas.getPosX(), cas.getPosY(),dimension , dimension);
                paneprincpal.add(labmur);
            }
            else if(casegauche && casedroite){
                /* Je dessine un mur noir avec un coté haut et bas */
                ImageIcon mur = new ImageIcon(new ImageIcon("image/Mur/MurHautBas.png").getImage().getScaledInstance(dimension, dimension, Image.SCALE_SMOOTH));
                JLabel labmur = new JLabel(mur);
                labmur.setBounds(cas.getPosX(), cas.getPosY(),dimension , dimension);
                paneprincpal.add(labmur);
            }
            else if(casegauche && casehaut){
                /* Je dessine un mur noir avec un coté bas et droit */
                ImageIcon mur = new ImageIcon(new ImageIcon("image/Mur/MurBasDroite.png").getImage().getScaledInstance(dimension, dimension, Image.SCALE_SMOOTH));
                JLabel labmur = new JLabel(mur);
                labmur.setBounds(cas.getPosX(), cas.getPosY(),dimension , dimension);
                paneprincpal.add(labmur);
            }
            else if(casegauche && casebas){
                /* Je dessine un mur noir avec un coté haut et droite */
                ImageIcon mur = new ImageIcon(new ImageIcon("image/Mur/MurHautDroite.png").getImage().getScaledInstance(dimension, dimension, Image.SCALE_SMOOTH));
                JLabel labmur = new JLabel(mur);
                labmur.setBounds(cas.getPosX(), cas.getPosY(),dimension , dimension);
                paneprincpal.add(labmur);
            }
            else if(casedroite && casehaut){
                /* Je dessine un mur noir avec un coté bas et gauche */
                ImageIcon mur = new ImageIcon(new ImageIcon("image/Mur/MurBasGauche.png").getImage().getScaledInstance(dimension, dimension, Image.SCALE_SMOOTH));
                JLabel labmur = new JLabel(mur);
                labmur.setBounds(cas.getPosX(), cas.getPosY(),dimension , dimension);
                paneprincpal.add(labmur);
            }
            else if(casedroite && casebas){
                /* Je dessine un mur noir avec un coté haut et un coté gauche */
                ImageIcon mur = new ImageIcon(new ImageIcon("image/Mur/MurHautGauche.png").getImage().getScaledInstance(dimension, dimension, Image.SCALE_SMOOTH));
                JLabel labmur = new JLabel(mur);
                labmur.setBounds(cas.getPosX(), cas.getPosY(),dimension , dimension);
                paneprincpal.add(labmur);
            }
            else if(casehaut && casebas){
                /* Je dessine un mur noir avec un coté gauche et un coté droite */
                ImageIcon mur = new ImageIcon(new ImageIcon("image/Mur/MurGaucheDroite.png").getImage().getScaledInstance(dimension, dimension, Image.SCALE_SMOOTH));
                JLabel labmur = new JLabel(mur);
                labmur.setBounds(cas.getPosX(), cas.getPosY(),dimension , dimension);
                paneprincpal.add(labmur);
            }
            else if(casebas){
                /* Je dessine un mur noir avec 3 coté sauf bas */
                ImageIcon mur = new ImageIcon(new ImageIcon("image/Mur/MurSaufBas.png").getImage().getScaledInstance(dimension, dimension, Image.SCALE_SMOOTH));
                JLabel labmur = new JLabel(mur);
                labmur.setBounds(cas.getPosX(), cas.getPosY(),dimension , dimension);
                paneprincpal.add(labmur);
            }
            else if(casehaut){
                /* Je dessine un mur noir avec 3 coté sauf haut */
                ImageIcon mur = new ImageIcon(new ImageIcon("image/Mur/MurSaufHaut.png").getImage().getScaledInstance(dimension, dimension, Image.SCALE_SMOOTH));
                JLabel labmur = new JLabel(mur);
                labmur.setBounds(cas.getPosX(), cas.getPosY(),dimension , dimension);
                paneprincpal.add(labmur);
            }
            else if(casegauche){
                ImageIcon mur = new ImageIcon(new ImageIcon("image/Mur/MurSaufGauche.png").getImage().getScaledInstance(dimension, dimension, Image.SCALE_SMOOTH));
                JLabel labmur = new JLabel(mur);
                labmur.setBounds(cas.getPosX(), cas.getPosY(),dimension , dimension);
                paneprincpal.add(labmur);
            }
            else if(casedroite){
                ImageIcon mur = new ImageIcon(new ImageIcon("image/Mur/MurSaufDroite.png").getImage().getScaledInstance(dimension, dimension, Image.SCALE_SMOOTH));
                JLabel labmur = new JLabel(mur);
                labmur.setBounds(cas.getPosX(), cas.getPosY(),dimension , dimension);
                paneprincpal.add(labmur);
                /* Je dessine un mur noir avec 3 coté sauf droite */
            }
            else{
                ImageIcon mur = new ImageIcon(new ImageIcon("image/Mur/MurAll.png").getImage().getScaledInstance(dimension, dimension, Image.SCALE_SMOOTH));
                JLabel labmur = new JLabel(mur);
                labmur.setBounds(cas.getPosX(), cas.getPosY(),dimension , dimension);
                paneprincpal.add(labmur);
            }


        }
        listeboulejauneun = new ArrayList();
        listeboulejaunedeux = new ArrayList();
        for(Case cas : this.getControl().getGrille().casePorte){
            ImageIcon porte = new ImageIcon(new ImageIcon("image/Mur/Porte.png").getImage().getScaledInstance(30, 30, Image.SCALE_SMOOTH));
            JLabel labPorte = new JLabel(porte);
            labPorte.setOpaque(false);
            labPorte.setBounds(cas.getPosX(), cas.getPosY(), dimension, dimension);
            paneprincpal.add(labPorte);
        }
        for (Case cas : this.getControl().getGrille().getCaseLibre()) {

              gomme = new ImageIcon(new ImageIcon("image/boule_jaune.png").getImage());
              labGomme = new JLabel(gomme);
              labGomme.setBounds((cas.getPosX()+cas.getPosX2())/2, (cas.getPosY()+cas.getPosY2())/2, 10, 10);
              test.add(labGomme);
               paneGrille.add(test);

              listeboulejauneun.add(cas);

          }


        bouleinvinciblepanel = new JPanel();
        bouleinvinciblepanel.setLayout(null);
        bouleinvinciblepanel.setBounds(0, 0, 1200, 1800);
        bouleinvinciblepanel.setPreferredSize(new Dimension(1200, 1800));
        bouleinvinciblepanel.setOpaque(false);
        bouleinvinciblepanel.setDoubleBuffered(true);
        bouleinvinciblepanel.setBackground(Color.black);


        for (Case cas : this.getControl().getGrille().casebouleinvincible) {

            gomme = new ImageIcon(new ImageIcon("image/bouleinvincible.png").getImage());
            labGomme = new JLabel(gomme);
            labGomme.setBounds(cas.getPosX(), cas.getPosY(), dimension, dimension);
            bouleinvinciblepanel.add(labGomme);
            paneGrille.add(bouleinvinciblepanel);

            //listeboulejauneun.add(cas);

        }

        paneprincpal.add(paneGrille);
        paneprincpal.add(score);
        cont.add(paneprincpal);
        this.addKeyListener(c);
        this.setVisible(true);


        }

    public Controleur getControl(){
        return this.c;
    }
}
