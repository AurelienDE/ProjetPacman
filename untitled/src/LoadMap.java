
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by willi on 24/02/2016.
 */
public class LoadMap {
    private static int sizecase = 30;

    public LoadMap(Grille g,int ... niveau) {
        /*
        570/1.5=380
        570*1.4=798
        570*2=1140
        660/1.5=440
        660*1.4=924
        660*2=1320
        30*2=60
        30*1.4=42
        30/1.5=20
         */
      //  try {
        //    this.RedefinirGrille(g);
        //} catch (IOException e) {
          //  e.printStackTrace();
       // }
     /*   System.out.println("Hauteur :"+g.hauteur);
        System.out.println("Largeur :"+g.largeur);
        System.out.println("MaxCaseH :"+g.maxCaseH);
        System.out.println("MaxCaseL :"+g.maxCaseL); */
        FileReader fr = null;
        File f = null;
        int cpt = 0;
        char[] maxCase=new char[2];
        try {
            try{
                //tmp contient le niveau actuel
                int tmp = Integer.parseInt(getMap().substring(getMap().length()-1,getMap().length()));
                tmp+=niveau[0];
                try {
                    String s = getMap().substring(0,getMap().length()-1);
                    File repertoire = new File("./map");
                    String[] children = repertoire.list();
                    int modulo = children.length;
                    int i = tmp%modulo;
                    if(i==0){
                        f = new File("./map/map"+modulo+".txt");
                    }
                    else {
                        f = new File("./map/" + s + "" + i + ".txt");
                    }
                }
                catch(Exception e3){
                    //String s = getMap().substring(0,getMap().length()-1);
                    //f = new File("./map/map1.txt");
                }
            }
            catch(Exception e10){
                f = new File("./map/"+getMap()+".txt");
            }
            System.out.println("FICHIEr : "+f.toString());
           // File f = new File("map/map.txt");
            fr = new FileReader(f);
            fr.read(maxCase,0,2);
            g.maxCaseL=Integer.parseInt(String.valueOf(maxCase));
            fr.skip(1);
            fr.read(maxCase,0,2);
            System.out.println(g.maxCaseH);
            g.maxCaseH=Integer.parseInt(String.valueOf(maxCase));
            fr.skip(1);
            try {
                this.RedefinirGrille(g);
                sizecase = g.getSizeCase();
                System.out.println("Hauteur :"+g.hauteur);
                System.out.println("Largeur :"+g.largeur);
                System.out.println("MaxCaseH :"+g.maxCaseH);
                System.out.println("MaxCaseL :"+g.maxCaseL);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException exception) {
            System.out.println("Le fichier n'a pas été trouvé");
        } catch (IOException e) {
            e.printStackTrace();
        }
        int c=0;
        int y1=0;
        int x1=0;
        int y2=g.getSizeCase();
        int x2=g.getSizeCase();
        int[] base=new int[20];
        int cptbase=0;
        int cptbase2=0;
        while (c != -1) {
            cpt++;
       //     System.out.print((char) c);
            try {
                c = fr.read();
               // System.out.print((char)c);
            } catch (IOException exception) {
                System.out.println("Erreur lors de la lecture : " + exception.getMessage());
            }
            if(c == 120){ //x
              //  System.out.print("Coucou"+(char)c);
                Case cas=new Case(x1,x2,y1,y2,1);
               // System.out.println(cas);
                g.getCaseOccuper().add(cas);
                g.getCase().add(cas);
                x1+=sizecase;
                x2+=sizecase;
            }
           /* else if(cpt==g.maxCaseH+4 || cpt==( g.maxCaseH*2 )+1 || cpt==g.maxCaseH*g.maxCaseL+g.maxCaseH+1
                    || cpt==g.maxCaseH*g.maxCaseL+4){
                System.out.print("Coucou"+(char)c);
                Case cas=new Case(x1,x2,y1,y2,5);
                g.getCase().add(cas);
                g.casebouleinvincible.add(cas);
                x1+=sizecase;
                x2+=sizecase;
            } */
            else if(c == 121){ //y
               // System.out.print("Coucou"+(char)c);
                Case cas=new Case(x1,x2,y1,y2,0);
                g.getCase().add(cas);
                boolean tmp = true;
                for(Case c2 : g.casePorte){
                    System.out.println(c2);
                    if(c2.getPosX()==x1 && c2.getPosY()+g.getSizeCase()==y1){
                        tmp=false;
                    }
                }
                if(tmp) {
                    g.getCaseLibre().add(cas);
                }
                x1+=sizecase;
                x2+=sizecase;
            }
            else if(c == 112){ //p
                Case cas=new Case(x1,x2,y1,y2,0);
                g.getCase().add(cas);
                g.casebegin=cas;
                g.getCaseLibre().add(cas);
                x1+=sizecase;
                x2+=sizecase;
            }
            else if(c == 119){ //w
                //  System.out.print("Coucou"+(char)c);
                Case cas=new Case(x1,x2,y1,y2,2);
                g.getCase().add(cas);
               // g.getCaseLibre().add(cas);
                x1+=sizecase;
                x2+=sizecase;

            }
            else if(c == 117){ //u
                Case cas=new Case(x1,x2,y1,y2,3);
                g.getCase().add(cas);
                g.casePorte.add(cas);
                base[cptbase]=cpt+g.maxCaseL-1;
                cptbase++;
                x1+=sizecase;
                x2+=sizecase;
            }
           else if(c == 118){ //v //23,39,422,438
                  System.out.print("Coucou"+(char)c);
                  Case cas=new Case(x1,x2,y1,y2,5);
                  g.getCase().add(cas);
                  g.casebouleinvincible.add(cas);
                  x1+=sizecase;
                 x2+=sizecase;

               }
            else{

            }

            if(x2>g.hauteur){
                x1=0;
                x2=g.getSizeCase();
                y1+=sizecase;
                y2+=sizecase;
            }
        }
    }
    public String getMap() throws IOException {
        File f = new File("./settings.txt");
        FileReader fr = new FileReader(f);
        int c = 0;
        String motsauvegarde = "";
        String map = "";
        final int ecart = 3;
        while (c != -1) {
            if (c == 32) {
                motsauvegarde = motsauvegarde.substring(0, motsauvegarde.length() - 1);
               // System.out.println(motsauvegarde);
               // System.out.println(motsauvegarde.length());
                if (motsauvegarde.equals("Map")) {
                    System.out.print("MATCH ok");
                    for (int i = 0; i < ecart; i++) {
                        c = fr.read();
                    }
                    while ((char) c != '\n') {
                        map += (char) c;
                        c = fr.read();
                    }
               //     System.out.println(map);
                    motsauvegarde = "";
                }
                c = fr.read();
                motsauvegarde += (char) c;
            }
            if((char) c =='\n'){
                motsauvegarde = "";
            }
            c = fr.read();
            motsauvegarde += (char) c;
        }
        System.out.println(map);
        System.out.println("fini");
        return map;
    }


    public void RedefinirGrille(Grille g) throws IOException {
        File f = new File("./settings.txt");
        FileReader fr = new FileReader(f);
        int c = 0;
        String motsauvegarde = "";
        String height = "";
        String width = "";
        final int ecart = 3;
        while (c != -1) {
            if (c == 32) {
                motsauvegarde=motsauvegarde.substring(0,motsauvegarde.length()-1);
                System.out.println(motsauvegarde);
                System.out.println(motsauvegarde.length());
                if (motsauvegarde.equals("Height")) {
                    System.out.print("MATCH ok");
                    for (int i = 0; i < ecart; i++) {
                        c = fr.read();
                    }
                    while ((char) c != '\n') {
                        height += (char) c;
                        c = fr.read();
                    }
                    System.out.println(height);
                    motsauvegarde = "";
                } else if (motsauvegarde.equals("Width")) {
                    System.out.print("MATCH ok");
                    for (int i = 0; i < ecart; i++) {
                        c = fr.read();
                    }
                    while ((char) c != '\n') {
                        width += (char) c;
                        c = fr.read();
                    }
                    System.out.println(width);
                    motsauvegarde = "";
                }
            }
            c = fr.read();
            motsauvegarde += (char) c;
        }
        if(height.equals("*0.5")){
            g.setSizeCase(20);
        }
        else if(height.equals("*1.5")){
            g.setSizeCase(42);
        }
        else{
            g.setSizeCase(30);
        }
        g.redefineGrille2(g.getSizeCase()*g.maxCaseH,g.getSizeCase()*g.maxCaseL);

    }
}

