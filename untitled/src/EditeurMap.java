

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by willi on 12/03/2016.
 */
public class EditeurMap {
    HashMap<String,Image> bloc;
    Grille g;
    Image visualisation;
    String blocselected;

    private static final int sizecase=30;
    public EditeurMap() {
        blocselected = "";
        bloc = new HashMap<String,Image>();
        File repertoire = new File("./image/Editeur");
        String[] children = repertoire.list();
        for(int i=0;i<children.length;i++){
            System.out.println(children[i]);
            try {
                BufferedImage image = ImageIO.read(new File("image/Editeur/"+children[i]));
              //  if(children[i].equals("PacmanDroiteO.png")){
               //     this.imagePacman=children[i].substring(0,children[i].length()-4);
           //     }
                bloc.put(children[i].substring(0,children[i].length()-4),image);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        g = null;
    }
    public void setGrille(int x, int y) {
        g = new Grille(x * sizecase, y * sizecase);
        setGrille2();
    }
    public void setGrille2() {
        //final int ecart = 1:
        int y1=0;
        int x1=0;
        int y2=30;
        int x2=30;
        for (int i1 = 0; i1 < g.maxCaseH; i1++) {
            if(i1!=0){
                x1= i1*sizecase;
                x2 = (i1+1) * sizecase;
            }
            for (int i = 0; i < g.maxCaseL; i++) {
                if (i != 0) {
                    y1=i*sizecase;
                    y2 = (i+1) * sizecase;
                }
               // Case c=new Case(x1+8,x2+8,y1+32,y2+32,0);
                Case c = new Case(y1,y2,x1,x2,0);
                g.getCase().add(c);
                g.getCaseLibre().add(c);
            }
            y1=0;
            y2=0;
        }
    }
    public void writeMap(Grille g){
        File repertoire = new File("./map");
        String[] children = repertoire.list();
        String link = "";
        int number = 0;
        for(int i=0;i<children.length;i++){
            System.out.println(children[i]);
            link=children[i].substring(0,children[i].length()-5);
            number=Integer.parseInt(children[i].substring(children[i].length()-5,children[i].length()-4));
            number+=1;
        }
        File f = new File("map/"+link+number+".txt");
        FileWriter fw=null;
        try {
            fw = new FileWriter(f);
            int h=0;
            int l=0;
            int cpt=0;
            int total=g.maxCaseH*g.maxCaseL;
            fw.write(g.maxCaseH+"*"+g.maxCaseL+"\n");
            while(cpt<total){
                if(cpt%g.maxCaseL==0 && cpt!=0){
                    fw.write('\n');
                    //System.out.println("");
                }
                if(g.getC(cpt).getType()==1){
                    fw.write("x");
                    //System.out.print("x");
                }
                else if(g.getC(cpt).getType()==0){
                    fw.write("y");
                    //System.out.print("y");
                }
                else if(g.getC(cpt).getType()==2){
                    fw.write("w");
                    //System.out.print("w");
                }
                else if(g.getC(cpt).getType()==3){
                    fw.write("u");
                   // System.out.print("u");
                }
                else if(g.getC(cpt).getType()==5){
                    fw.write("v");
                 //   System.out.print("v");
                }
                else if(g.getC(cpt).getType()==4){
                    fw.write("p");
                }

                cpt++;
            }
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
