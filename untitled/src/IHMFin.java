/**
 * Created by aurelien on 06/03/16.
 */


import javax.swing.*;
import java.awt.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;


public class IHMFin extends JFrame {

    Container cont;
    MyButton rejouer;
    MyButton envoyerscore;
    JTextField pseudo;
    static BufferedWriter out=null;
    int score;

    public IHMFin(int score) {

        this.score = score;
        this.setTitle("PACMAN");
        this.setSize(630, 730);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setBackground(Color.black);

        cont = this.getContentPane();


        JPanel panel= new JPanel();

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS)) ;


        ControleurDebut o = new ControleurDebut(null,null,this,null);


        JLabel affscore= new JLabel ("Votre Score : " +score);


        rejouer = new MyButton("Rejouer","image/btn.png","");
        rejouer.setActionCommand("lienrejouer");
        rejouer.addActionListener(o);

        pseudo =  new JTextField("Jean Michel");

        envoyerscore = new MyButton("Sauvegarder score","image/btn.png","imge/fondblanc.png");
        envoyerscore.setActionCommand("lienenvoyerscore");
        envoyerscore.addActionListener(o);


        JPanel videhaut=new JPanel();
        JPanel affscorepanel = new JPanel();
        JPanel rejouerpanel= new JPanel();
        JPanel pseudopanel= new JPanel();
        JPanel envoyerscorepanel= new JPanel();
        JPanel videbas=new JPanel();

        affscore.setPreferredSize(new Dimension(200, 40));
        rejouer.setPreferredSize(new Dimension(200, 40));
        pseudo.setPreferredSize(new Dimension(200, 40));
        envoyerscore.setPreferredSize(new Dimension(200, 40));

        videhaut.setBackground(Color.black);
        affscorepanel.setBackground(Color.black);
        rejouerpanel.setBackground(Color.black);
        pseudopanel.setBackground(Color.black);
        envoyerscorepanel.setBackground( Color.black );
        videbas.setBackground(Color.black);

        affscorepanel.add(affscore);
        rejouerpanel.add(rejouer);
        pseudopanel.add(pseudo);
        envoyerscorepanel.add(envoyerscore);

        panel.add(videhaut);
        panel.add(affscorepanel);
        panel.add(rejouerpanel);
        panel.add(pseudopanel);
        panel.add(envoyerscorepanel);
        panel.add(videbas);

        cont.add(panel);

        this.setVisible(true);

    }
}
