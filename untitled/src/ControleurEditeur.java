
//import com.intellij.ide.UiActivity;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by willi on 12/03/2016.
 */
public class ControleurEditeur implements ActionListener, MouseListener, MouseMotionListener,FocusListener{
    EditeurMap em;
    boolean valider = true;
    public ControleurEditeur(){
        em = new EditeurMap();
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand()=="ValideCase" && valider){
            int x=10;
            int y=10;
            try {
                x = Integer.parseInt(ControleurDebut.fe.hauteur.getText());
                y = Integer.parseInt(ControleurDebut.fe.largeur.getText());
                if(x>24 || y>24){
                    throw new Exception("TropgrandException");
                }
                em.setGrille(x,y);
                getFenetre().AjouterGrille();
                valider=false;
            }
            catch(Exception e1){
                if(e1.getLocalizedMessage().equals("TropgrandException")){
                    getFenetre().erreur.setText("Veuillez entrez un nombre <24 et > 10");
                }
                else{
                    getFenetre().erreur.setText("Veuillez entrer des nombres correctes");
                }
                getFenetre().erreur.setVisible(true);
            }

           // ControleurDebut.fe.paintComponents(ControleurDebut.fe.getGraphics());
           // ControleurDebut.fe.paintComponents(ControleurDebut.fe.paneVisual.getGraphics());
        }
        if(e.getActionCommand()=="Terminer" && !valider){
            em.writeMap(em.g);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        boolean select = false;
        String unselect = em.blocselected;
        JLabel cmp = (JLabel)e.getComponent();
        for(String s : getFenetre().bloclabel.keySet()){
            if(getFenetre().bloclabel.get(s).getIcon()==cmp.getIcon()){
                //cmp.setBorder(getFenetre().clbselect);
                em.blocselected=s;
            }
            else if(s.equals(unselect)){
                //getFenetre().bloclabel.get(s).setBorder(getFenetre().clbunselect);
            }
        }
        if(em.blocselected.equals(unselect)){
            //getFenetre().bloclabel.get(em.blocselected).setBorder(getFenetre().clbunselect);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        System.out.println("released");
        //getFenetre().paneVisual.setVisible(false);
        //getFenetre().paneVisual.setVisible(true);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        boolean diff = true;
        System.out.println("case");
        for(String s : getFenetre().bloclabel.keySet()) {
            if (e.getComponent() == getFenetre().bloclabel.get(s)) {
                diff = false;
            }
        }
        if(e.isShiftDown()) { // Shift appuyer

            //  if(e.getComponent()!=getFenetre().labimage) {
            if (diff == true){
                //((JLabel) e.getComponent()).setIcon(getFenetre().bloclabel.get(em.blocselected).getIcon());
                Icon i = getFenetre().bloclabel.get(em.blocselected).getIcon();
                ImageIcon ic = new ImageIcon(((ImageIcon)i).getImage().getScaledInstance(30,30,Image.SCALE_SMOOTH));
                ((JLabel) e.getComponent()).setIcon(ic);
                Case c=null;
                for(Integer in : getFenetre().grille.keySet()){
                    if(getFenetre().grille.get(in)==e.getComponent()){
                       c= em.g.getC(in);
                    }
                }
                if(em.blocselected.substring(0,3).equals("Mur")) {
                    c.setType(1);
                }
                else if(em.blocselected.substring(0,3).equals("tel")){
                    c.setType(2);
                }
                else if(em.blocselected.substring(0,3).equals("Por")){
                    c.setType(3);
                }
                else if(em.blocselected.substring(0,3).equals("bou")){
                    c.setType(5);
                }
                else if(em.blocselected.substring(0,3).equals("Pac")){
                    c.setType(4);
                }
            }
        }
        if(e.isControlDown()){
            if(diff==true) {
                Case c=null;
                for(Integer in : getFenetre().grille.keySet()){
                    if(getFenetre().grille.get(in)==e.getComponent()){
                        c= em.g.getC(in);
                    }
                }
                c.setType(0);
                ((JLabel) e.getComponent()).setIcon(getFenetre().blancimage);
            }
        }

        //System.out.println((JLabel)e.getComponent().setIcon());
    }

    @Override
    public void mouseExited(MouseEvent e) {
      /*  if(!e.isShiftDown()){
            if(e.getComponent().!=getFenetre().labimage){
                if(((JLabel)e.getComponent()).getIcon()!=getFenetre().blancimage);
                ((JLabel)e.getComponent()).setIcon(getFenetre().blancimage);
            }

        }*/
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        /* L'image que ta sélectioner
       // System.out.println(e.getSource());
       */
       // System.out.println(e.getPoint());
       // System.out.println(e.getX());
       // System.out.println(e.getY());
        /* Screen mon ordi */
        /*System.out.println(e.getLocationOnScreen());
        System.out.println(e.getXOnScreen()-getFenetre().getX());
        if(e.getXOnScreen()-getFenetre().getX()<750){
            //getFenetre().paneOutils.remove(getFenetre().labimage);
            //getFenetre().paneOutils.validate();
            //getFenetre().paneOutils.repaint();
          //  getFenetre().cont.add(getFenetre().labimage);
            getFenetre().labimage.setBounds(e.getXOnScreen()-getFenetre().getX(),
                   e.getYOnScreen()-getFenetre().getY(),30,30);
        } */



    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
    public FenetreEdit getFenetre(){
        return ControleurDebut.fe;   }

    @Override
    public void focusGained(FocusEvent e) {
        ((JTextArea)e.getComponent()).setText("");
        if(getFenetre().erreur.isVisible()){
            getFenetre().erreur.setVisible(false);
        }
    }

    @Override
    public void focusLost(FocusEvent e) {

    }
}
