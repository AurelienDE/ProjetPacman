public class Pacman{
    private int posX;
    private int posY;
    private String nom;
    private String image[];
    private boolean mort;
    private int direction;
    /*
        X correspond a la largeur.
        Y correspond a la hauteur.
     */
    public Pacman(int p1,int p2, String nom){
        this.posX=p1;
        this.posY=p2;
        this.nom=nom;
        this.mort=false;
        this.direction=0;
    }
    public boolean getMort(){
        return this.mort;
    }
    public void Mort(){
        this.mort= true;
    }
    public int getDirection(){
        return this.direction;
    }
    public void setDirection(int d){
        this.direction=d;
    }
    public void incrementePosX(){
        posX+=15;
    }
    public void incrementePosY(){
        posY+=15;
    }
    public void decrementePosY(){
        posY-=15;
    }
    public void decrementePosX(){
        posX-=15;
    }
    public int getPosX(){
        return this.posX;
    }
    public int getPosY(){
        return this.posY;
    }
    public String toString(){
        return "Pacman X : "+getPosX()+" Y : "+getPosY()+".";
    }
}